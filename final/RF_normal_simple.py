#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug 12 01:47:50 2018

@author: ckadar
"""

import sys
import pandas as pd
import numpy as np

from sklearn.preprocessing import StandardScaler
from sklearn.ensemble import RandomForestClassifier
from sklearn.grid_search import GridSearchCV


data_path = 'dataset_final.csv'
RF_normal_proba_path = "proba_RF_normal_simple_all_features.csv"
RF_normal_features_path = "feat_impo_RF_normal_simple_all_features.npy"
RF_normal_proba_training_path = "proba_training_RF_normal_simple_all_features.csv"

out_path = "RF_normal_simple_out.txt"

def feature_imp(avg_impo, avg_impo_std, feat_names):
    print('Feature ranking...')
    indices = np.argsort(avg_impo)[::-1]
    impo_list = []
    
    for f in range(len(feat_names)):
        print("%d. Feature: %s (%f)" % (f + 1, feat_names[indices[f]], avg_impo[indices[f]])), 'STD: ', avg_impo_std[indices[f]]
        impo_list.append([feat_names[indices[f]], avg_impo[indices[f]], avg_impo_std[indices[f]]]) 
        
    # plot the feature importances of the forest
    
    #plt.figure(figsize=(30,8))
    #plt.title("Feature importances")
    #plt.bar(range(len(feat_names)), avg_impo[indices],color="r", yerr=avg_impo_std[indices], align="center")
    #plt.xticks(range(len(feat_names)), feat_names[indices])
    #plt.xlim([-1,20])
    
    return impo_list

def load_data():
    ##%%
    print('---------------------------------')
    print("Reading in data...")
    data = pd.read_csv(data_path,index_col=[1,2]).drop(['Unnamed: 0'],axis=1)  
    print(data.info)
    ##%%

    print(data.shape)
    ##(11123304, 72)
    
    all_columns = data.columns
    print(all_columns)
    
    features = data.drop(['offence','probability','offence_ids','pred_class','probability_norm','pred_class_norm','pred_class_bin','to_date'],axis=1)
    
    target = data['to_date']
    target = target.astype(int)

    print('Features shape: ',features.shape,'Target shape: ', target.shape)
    ##Features shape:  (11123304, 64) Target shape:  (11123304,)
    ##%%
    
    feat_names = features.columns
    
    print('Total Number of cells: ', features.loc['2014-01-14',:].shape[0])
    print('Total Number of days: ', features.loc[(slice('2014-01-14','2017-01-13'),1429),:].shape[0])
    print('Number of training days: ', features.loc[(slice('2014-01-14','2016-01-13'),1429),:].shape[0])
    print('Number of testing days: ', features.loc[(slice('2016-01-14','2017-01-13'),1429),:].shape[0])
    ##Total Number of cells:  10149
    ##Total Number of days:  1096
    ##Number of training days:  730
    ##Number of testing days:  366
    
    print(feat_names)
    print(len(feat_names))
    
    ##%%
    print ('Class distributions - Original dataset of built land-use')
    print('Class 0: ', target.value_counts()[0], '(',float(target.value_counts()[0])/target.shape[0]*100, '% )')
    print('Class 1: ', target.value_counts()[1], '(',float(target.value_counts()[1])/target.shape[0]*100, '% )')
    ##Class 0:  11117038 ( 99.94366781668468 % )
    ##Class 1:  6266 ( 0.0563321833153171 % )
    
    ## split dataset into training and testing data
    X_train = features.loc['2014-01-14':'2016-01-13',:].values
    y_train = target.loc['2014-01-14':'2016-01-13',:].values
    X_test = features.loc['2016-01-14':'2017-01-13',:].values
    y_test = target.loc['2016-01-14':'2017-01-13',:].values

    tot = len(y_train)
    print('Class distributions - Training')
    print('Class 0: ', np.count_nonzero(y_train == 0), '(',float(np.count_nonzero(y_train == 0))/tot*100, '% )')
    print('Class 1: ', np.count_nonzero(y_train == 1), '(',float(np.count_nonzero(y_train == 1))/tot*100, '% )')
    ##Class 0:  7404388 ( 99.94085387992878 % )
    ##Class 1:  4382 ( 0.05914612007121291 % )
    
    tot = len(y_test)
    print('Class distributions - Testing ')
    print('Class 0: ', np.count_nonzero(y_test == 0), '(',float(np.count_nonzero(y_test == 0))/tot*100, '% )')
    print('Class 1: ', np.count_nonzero(y_test == 1), '(',float(np.count_nonzero(y_test == 1))/tot*100, '% )')
    ##Class 0:  3712650 ( 99.94928031349289 % )
    ##Class 1:  1884 ( 0.05071968650710
    
    return X_train,y_train,X_test,y_test,feat_names

def main():
    sys.stdout = open(out_path, 'w')
    
    print("DOING DATA READING")
    sys.stdout.flush()
    #read in data
    X_train, y_train, X_test, y_test, all_features = load_data()
    print(sum(y_test))

    
    print("DOING DATA SCALING")
    sys.stdout.flush()
    scaling = StandardScaler()
    #for non-DT based models we need to do feature scalling
    X_train = scaling.fit_transform(X_train) ##here use fit and transform
    X_test = scaling.transform(X_test)
    
   
    # define grid containing a set of hyperparameters
    feature_count = X_train.shape[1]
    print(feature_count)
    
    param_grid = { 'n_estimators': [100]
                  #'n_estimators': [500, 1000, 1500],
                  #'max_depth': [int(feature_count/3), int(feature_count/2), None]
                  #'max_features': ['auto', 'sqrt', 'log2']
                }

    print("DOING ML TRAINING")
    sys.stdout.flush()
    

    # define the estimator
    clf = RandomForestClassifier(n_jobs=-1)
    #clf = RandomForestClassifier(n_jobs=-1)

    # define the CV estimator
    CV_clf = GridSearchCV(estimator=clf, param_grid=param_grid, cv= 5, scoring='roc_auc')

    # fit the CV estimator
    CV_clf.fit(X_train,y_train)
    
    #extract best score, best parameters, best model of the CV on this subset
    RF_best_score = CV_clf.best_score_
    print('        The best score is: ', RF_best_score)
    
    RF_best_params = CV_clf.best_params_
    print('        The best parameters are: ',RF_best_params)
    
    RF_best_model = CV_clf.best_estimator_
    print('        The best model is: ', RF_best_model)
    sys.stdout.flush()
    
    #and test it on the test set
    #y_preds = best_model.predict(X_test)
    RF_y_preds_proba = RF_best_model.predict_proba(X_test)

    #test_score = roc_auc_score(y_test,y_preds)
    #append the score of the best model on the test set
    #test_scores.append(test_score) 
    #print(test_score)
    
            
    #  feature importance values extracted during CV
    RF_importances = RF_best_model.feature_importances_
    RF_importances_std = np.std([tree.feature_importances_ for tree in RF_best_model.estimators_],axis=0)

    #test order of classes
    #print(CV_clf.classes_)

    print("DOING DATA SAVING")
    sys.stdout.flush()
    # save  probabilities 
    np.savetxt(RF_normal_proba_path, RF_y_preds_proba, delimiter=",")

    # save feature importance + STD + name
    print(all_features)
    RF_features_list = feature_imp(RF_importances, RF_importances_std, all_features)
    np.save(RF_normal_features_path,RF_features_list)

      
    #save also predictions on training set -- actually on the subsample!!!
    RF_y_training_proba = RF_best_model.predict_proba(X_train) 
    np.savetxt(RF_normal_proba_training_path, RF_y_training_proba, delimiter=",")

main()

	