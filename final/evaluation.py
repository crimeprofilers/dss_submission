#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 20 12:04:49 2018

@author: ckadar
"""
import pandas as pd
import numpy as np
import sklearn.linear_model as sklm
import sklearn.ensemble as skens
import sklearn.model_selection as skms
from sklearn.metrics import roc_curve, auc, confusion_matrix
from sklearn.ensemble import RandomForestClassifier
from sklearn.cross_validation import cross_val_score
from sklearn.metrics import roc_auc_score, roc_curve, auc, confusion_matrix, fbeta_score, make_scorer
from sklearn.grid_search import GridSearchCV

import matplotlib.pyplot as plt
from collections import Counter
import itertools
from tqdm import tqdm

from imblearn.under_sampling import RandomUnderSampler, NearMiss, ClusterCentroids
from imblearn.over_sampling import SMOTE 
from imblearn.datasets import make_imbalance
from imblearn.ensemble import BalancedBaggingClassifier
from imblearn.metrics import classification_report_imbalanced
from sklearn.ensemble import BaggingClassifier
from imblearn.ensemble import EasyEnsemble

data_path = 'dataset_final.csv'

dummy_coverage_path = "dummy_coverage.csv"

RF_normal_proba_path = "euler/proba_RF_normal_all_features.csv"
RF_normal_coverage_path = "euler/coverage_RF_normal_all_features.csv"

RF_costsensitive_proba_path = "euler/proba_RF_costsensitive_all_features.csv"
RF_costsensitive_coverage_path = "euler/coverage_RF_costsensitive_all_features.csv"

RF_heuristic_oversampling_proba_path = "euler/proba_RF_heuristic_oversampling_all_features.csv"
RF_heuristic_oversampling_coverage_path = "euler/coverage_RF_heuristic_oversampling_all_features.csv"

RF_oversampling_proba_path = "euler/proba_RF_oversampling_all_features.csv"
RF_oversampling_coverage_path = "euler/coverage_RF_oversampling_all_features.csv"
#%%
RF_normal_simple_proba_path = "proba_RF_normal_simple_all_features.csv"
RF_normal_simple_coverage_path = "coverage_RF_normal_simple_all_features.csv"

AB_ensemble_proba_path = "avg_test_proba_AB_ensemble_all_features.csv"
AB_ensemble_coverage_path = "coverage_AB_ensemble_all_features.csv"

LR_ensemble_proba_path = "avg_test_proba_LR_ensemble_all_features.csv"
LR_ensemble_coverage_path = "coverage_LR_ensemble_all_features.csv"

LRl1_ensemble_proba_path = "avg_test_proba_LRl1_ensemble_all_features.csv"
LRl1_ensemble_coverage_path = "coverage_LRl1_ensemble_all_features.csv"
####################################################################
proba_onlycrime1 ="1avg_test_proba_RF_ensemble_onlycrime_features.csv"
features_onlycrime1= "1feat_impo_RF_ensemble_onlycrime_features.npy"
proba_onlycrime2 ="2avg_test_proba_RF_ensemble_onlycrime_features.csv"
features_onlycrime2= "2feat_impo_RF_ensemble_onlycrime_features.npy"

proba0 = "0avg_test_proba_RF_ensemble_all_features.csv"
features0 = "0feat_impo_RF_ensemble_all_features.npy "
path0 = "0coverage_RF_ensemble_all_features.csv"

proba_onlycrime0 ="0avg_test_proba_RF_ensemble_onlycrime_features.csv"
features_onlycrime0= "0feat_impo_RF_ensemble_onlycrime_features.npy"
path_onlycrime0 = "0coverage_RF_ensemble_onlycrime_features.csv" 

proba_onlyspatial0 ="0avg_test_proba_RF_ensemble_onlyspatial_features.csv"
features_onlyspatial0= "0feat_impo_RF_ensemble_onlyspatial_features.npy"
path_onlyspatial0 = "0coverage_RF_ensemble_onlyspatial_features.csv" 

proba_onlytemporal0 ="0avg_test_proba_RF_ensemble_onlytemporal_features.csv"
features_onlytemporal0= "0feat_impo_RF_ensemble_onlytemporal_features.npy"
path_onlytemporal0 = "0coverage_RF_ensemble_onlytemporal_features.csv" 
####################################################################

proba1 = "1avg_test_proba_RF_ensemble_all_features.csv"
features1 = "1feat_impo_RF_ensemble_all_features.npy "
path1 = "1coverage_RF_ensemble_all_features.csv"

proba_onlycrime1 ="1avg_test_proba_RF_ensemble_onlycrime_features.csv"
features_onlycrime1= "1feat_impo_RF_ensemble_onlycrime_features.npy"
path_onlycrime1 = "1coverage_RF_ensemble_onlycrime_features.csv" 

proba_onlyspatial1 ="1avg_test_proba_RF_ensemble_onlyspatial_features.csv"
features_onlyspatial1= "1feat_impo_RF_ensemble_onlyspatial_features.npy"
path_onlyspatial1 = "1coverage_RF_ensemble_onlyspatial_features.csv" 

proba_onlytemporal1 ="1avg_test_proba_RF_ensemble_onlytemporal_features.csv"
features_onlytemporal1 = "1feat_impo_RF_ensemble_onlytemporal_features.npy"
path_onlytemporal1 = "1coverage_RF_ensemble_onlytemporal_features.csv" 
####################################################################

proba2 = "2avg_test_proba_RF_ensemble_all_features.csv"
features2 = "2feat_impo_RF_ensemble_all_features.npy "
path2 = "2coverage_RF_ensemble_all_features.csv"

proba_onlycrime2 ="2avg_test_proba_RF_ensemble_onlycrime_features.csv"
features_onlycrime2= "2feat_impo_RF_ensemble_onlycrime_features.npy"
path_onlycrime2 = "2coverage_RF_ensemble_onlycrime_features.csv" 

proba_onlyspatial2 ="2avg_test_proba_RF_ensemble_onlyspatial_features.csv"
features_onlyspatial2 = "2feat_impo_RF_ensemble_onlyspatial_features.npy"
path_onlyspatial2 = "2coverage_RF_ensemble_onlyspatial_features.csv" 

proba_onlytemporal2 ="2avg_test_proba_RF_ensemble_onlytemporal_features.csv"
features_onlytemporal2 = "2feat_impo_RF_ensemble_onlytemporal_features.npy"
path_onlytemporal2 = "2coverage_RF_ensemble_onlytemporal_features.csv" 
####################################################################

ensemble_probas_path = "proba_ensemble_ensemble_all_features.npy"
stacking_ensemble_proba_path = "avg_test_proba_stacking_ensemble_all_features.csv"
stacking_ensemble_coverage_path = "coverage_stacking_ensemble_all_features.csv"

RF_ensemble_proba_path = "avg_test_proba_RF_ensemble_all_features.csv"
RF_ensemble_features_path = "feat_impo_RF_ensemble_all_features.npy"
RF_ensemble_coverage_path = "coverage_RF_ensemble_all_features.csv"

LR_stacking_proba_path = "proba_LR_stacking_undersampling_all_features.csv"

#RF_costsensitive_proba_path = "proba_RF_costsensitive_all_features.csv"
#RF_costsensitive_features_path = "feat_impo_RF_costsensitive_all_features.npy"
#RF_costsensitive_coverage_path = "coverage_RF_costsensitive_all_features.csv"

#RF_heuristic_oversampling_proba_path = "proba_RF_heuristic_oversampling_all_features.csv"
#RF_heuristic_oversampling_features_path = "feat_impo_RF_heuristic_oversampling_all_features.npy"
#RF_heuristic_oversampling_coverage_path = "coverage_RF_heuristic_oversampling_all_features.csv"

#RF_oversampling_proba_path = "proba_RF_oversampling_all_features.csv"
#RF_oversampling_features_path = "feat_impo_RF_oversampling_all_features.npy"
#RF_oversampling_coverage_path = "coverage_RF_oversampling_all_features.csv"

RF_undersampling_proba_path = "proba_RF_undersampling_all_features.csv"
RF_undersampling_features_path = "feat_impo_RF_undersampling_all_features.npy"
RF_undersampling_coverage_path = "coverage_RF_undersampling_all_features.csv"

AB_undersampling_proba_path = "proba_AB_undersampling_all_features.csv"
AB_undersampling_features_path = "feat_impo_AB_undersampling_all_features.npy"
AB_undersampling_coverage_path = "coverage_AB_undersampling_all_features.csv"

LR_undersampling_proba_path = "proba_LR_undersampling_all_features.csv"
LR_undersampling_coverage_path = "coverage_LR_undersampling_all_features.csv"

LRl1_undersampling_proba_path = "proba_LRl1_undersampling_all_features.csv"
LRl1_undersampling_coverage_path = "coverage_LRl1_undersampling_all_features.csv"

RF_heuristic_undersampling_proba_path = "proba_RF_heuristic_undersampling_all_features.csv"
RF_heuristic_undersampling_features_path = "feat_impo_RF_heuristic_undersampling_all_features.npy"
RF_heuristic_undersampling_coverage_path = "coverage_RF_heuristic_undersampling_all_features.csv"

RF_ensemble_proba_path_only_crime = "avg_test_proba_RF_ensemble_onlycrime_features.csv"
RF_ensemble_features_path_only_crime = "feat_impo_RF_ensemble_onlycrime_features.npy"
RF_ensemble_coverage_path_only_crime = "coverage_RF_ensemble_only_crime.csv"

RF_ensemble_proba_path_only_spatial = "avg_test_proba_RF_ensemble_onlyspatial_features.csv"
RF_ensemble_features_path_only_spatial = "feat_impo_RF_ensemble_onlyspatial_features.npy"
RF_ensemble_coverage_path_only_spatial = "coverage_RF_ensemble_only_spatial.csv"

RF_ensemble_proba_path_only_temporal = "avg_test_proba_RF_ensemble_onlytemporal_features.csv"
RF_ensemble_features_path_only_temporal = "feat_impo_RF_ensemble_onlytemporal_features.npy"
RF_ensemble_coverage_path_only_temporal = "coverage_RF_ensemble_only_temporal.csv"

def only_load_data():
    ##%%
    print('---------------------------------')
    print("Reading in data...")
    data = pd.read_csv(data_path,index_col=[1,2]).drop(['Unnamed: 0'],axis=1)  
    print(data.info)
    ##%%

    print(data.shape)
    ##(11123304, 72)
    
    all_columns = data.columns
    print(all_columns)
    
    print('adding urb_lev column...')
    data['urb_lev'] = np.zeros(data.shape[0]).astype(int)
    # assign level of urbanization according to natural breaks thresholds
    #   jenks algorithm: [0.0, 18.25, 54.25, 250.75]
    #   percentiles: [0.0 2.25 16.75 250.75]
    print('   PERCENTILES')
    print('   33%: ', np.percentile(data.popdens, 33.33))
    print('   66%: ', np.percentile(data.popdens, 66.66))
    data.loc[(data['popdens'] >= np.percentile(data.popdens, 33.33)) & (data['popdens'] < np.percentile(data.popdens, 66.66)), 'urb_lev'] = 1
    data.loc[(data['popdens'] >= np.percentile(data.popdens, 66.66)), 'urb_lev'] = 2

    return data

def split_data(data, urb_level): 
    # only one type of urbanization level!
    features = data[data['urb_lev']==urb_level].drop(['offence','probability','offence_ids','pred_class','probability_norm','pred_class_norm','pred_class_bin','urb_lev','to_date'],axis=1)
    target = data[data['urb_lev']==urb_level].to_date
    target = target.astype(int)
    
    temporal_features = ['dow', 'holiday', 'event', 'temp', 'hum', 'discomf', 'daylight', 'moon']
    
    prior_features = ['prior1d', 'prior3d', 'prior7d', 'prior14d']

    poi_infra_features = ['buildgs_areafrac', 'buildgs_dens', 'land_swim', 'land_oldtown',
       'land_green', 'land_business', 'land_indust', 'land_central',
       'land_public', 'land_park', 'land_special', 'land_river', 'land_resi1',
       'land_resi2', 'land_resi3', 'land_mix2', 'land_mix3', 'land_no_use',
       'land_dividx', 'poi_infra', 'poi_shop', 'poi_gastro', 'poi_public',
       'poi_edu', 'intersection', 'highway_exit', 'border_cross', 'road_type',
       'pub_trans', 'pub_hous']
       
    demographic_features = [ 'popage_1', 'popage_2',
       'popage_3', 'popage_4', 'popage_dividx', 'popbirth_nonCH', 'popcit_CH',
       'popcit_EU', 'popcit_dividx', 'popcit_europ', 'popcit_noneurop',
       'popdens', 'popmale', 'popstab', 'busi_sec1', 'busi_sec2', 'busi_sec3',
       'busidens', 'busisec_dividx', 'empldens', 'emplmale', 'emplsec_dividx']

    features = features[prior_features]

    print('Features shape: ',features.shape,'Target shape: ', target.shape)
    ##Features shape:  (11123304, 64) Target shape:  (11123304,)
    ##%%
    
    feat_names = features.columns
    
    print('Total Number of cells: ', features.loc['2014-01-14',:].shape[0])
    
    print(feat_names)
    print(len(feat_names))
    
    ##%%
    print ('Class distributions - Original dataset of built land-use')
    print('Class 0: ', target.value_counts()[0], '(',float(target.value_counts()[0])/target.shape[0]*100, '% )')
    print('Class 1: ', target.value_counts()[1], '(',float(target.value_counts()[1])/target.shape[0]*100, '% )')
    ##Class 0:  11117038 ( 99.94366781668468 % )
    ##Class 1:  6266 ( 0.0563321833153171 % )
    
    ## split dataset into training and testing data
    X_train = features.loc['2014-01-14':'2016-01-13',:].values
    y_train = target.loc['2014-01-14':'2016-01-13',:].values
    X_test = features.loc['2016-01-14':'2017-01-13',:].values
    y_test = target.loc['2016-01-14':'2017-01-13',:].values

    tot = len(y_train)
    print('Class distributions - Training')
    print('Class 0: ', np.count_nonzero(y_train == 0), '(',float(np.count_nonzero(y_train == 0))/tot*100, '% )')
    print('Class 1: ', np.count_nonzero(y_train == 1), '(',float(np.count_nonzero(y_train == 1))/tot*100, '% )')
    ##Class 0:  7404388 ( 99.94085387992878 % )
    ##Class 1:  4382 ( 0.05914612007121291 % )
    
    tot = len(y_test)
    print('Class distributions - Testing ')
    print('Class 0: ', np.count_nonzero(y_test == 0), '(',float(np.count_nonzero(y_test == 0))/tot*100, '% )')
    print('Class 1: ', np.count_nonzero(y_test == 1), '(',float(np.count_nonzero(y_test == 1))/tot*100, '% )')
    ##Class 0:  3712650 ( 99.94928031349289 % )
    ##Class 1:  1884 ( 0.05071968650710
    
    return X_train,y_train,X_test,y_test,feat_names
#%%
def plot_data(data):
    fig = plt.figure(figsize=(5,5))
    ax = fig.add_subplot(111)
    cax = ax.matshow(data) 
    fig.colorbar(cax)
    plt.axis('equal')
    plt.axis('off')
    plt.show()

    #%%
def ROC_curve(pred_class1,y_test, label, color, symbol):
    false_positive_rate, true_positive_rate, thresholds = roc_curve(y_test, pred_class1)
    roc_auc = auc(false_positive_rate, true_positive_rate)
    plt.plot(false_positive_rate, true_positive_rate, linewidth=2, label= label + 'AUC = %0.3f'% roc_auc, color = color, ls = symbol)
    plt.legend(loc='lower right', fontsize=10)
    plt.xlim([-0.1,1.1])
    plt.ylim([-0.1,1.1])
    plt.ylabel('True Positive Rate')
    plt.xlabel('False Positive Rate')

    #%%
def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')


def load_data():
    ##%%
    print('---------------------------------')
    print("Reading in data...")
    data = pd.read_csv(data_path,index_col=[1,2]).drop(['Unnamed: 0'],axis=1)  
    print(data.info)
    ##%%

    print(data.shape)
    ##(11123304, 72)
    
    all_columns = data.columns
    print(all_columns)
    
    features = data.drop(['offence','probability','offence_ids','pred_class','probability_norm','pred_class_norm','pred_class_bin','to_date'],axis=1)
    
    target = data['to_date']
    target = target.astype(int)

    print('Features shape: ',features.shape,'Target shape: ', target.shape)
    ##Features shape:  (11123304, 64) Target shape:  (11123304,)
    ##%%
    
    feat_names = features.columns
    
    #1429 = 
    print('Total Number of cells: ', features.loc['2014-01-14',:].shape[0])
    print('Total Number of days: ', features.loc[(slice('2014-01-14','2017-01-13'),1429),:].shape[0])
    print('Number of training days: ', features.loc[(slice('2014-01-14','2016-01-13'),1429),:].shape[0])
    print('Number of testing days: ', features.loc[(slice('2016-01-14','2017-01-13'),1429),:].shape[0])
    ##Total Number of cells:  10149
    ##Total Number of days:  1096
    ##Number of training days:  730
    ##Number of testing days:  366
    
    print(feat_names)
    print(len(feat_names))
    
    ##%%
    print ('Class distributions - Original dataset of built land-use')
    print('Class 0: ', target.value_counts()[0], '(',float(target.value_counts()[0])/target.shape[0]*100, '% )')
    print('Class 1: ', target.value_counts()[1], '(',float(target.value_counts()[1])/target.shape[0]*100, '% )')
    ##Class 0:  11117038 ( 99.94366781668468 % )
    ##Class 1:  6266 ( 0.0563321833153171 % )
    
    ## split dataset into training and testing data
    X_train = features.loc['2014-01-14':'2016-01-13',:].values
    y_train = target.loc['2014-01-14':'2016-01-13',:].values
    X_test = features.loc['2016-01-14':'2017-01-13',:].values
    y_test = target.loc['2016-01-14':'2017-01-13',:].values

    tot = len(y_train)
    print('Class distributions - Training')
    print('Class 0: ', np.count_nonzero(y_train == 0), '(',float(np.count_nonzero(y_train == 0))/tot*100, '% )')
    print('Class 1: ', np.count_nonzero(y_train == 1), '(',float(np.count_nonzero(y_train == 1))/tot*100, '% )')
    ##Class 0:  7404388 ( 99.94085387992878 % )
    ##Class 1:  4382 ( 0.05914612007121291 % )
    
    tot = len(y_test)
    print('Class distributions - Testing ')
    print('Class 0: ', np.count_nonzero(y_test == 0), '(',float(np.count_nonzero(y_test == 0))/tot*100, '% )')
    print('Class 1: ', np.count_nonzero(y_test == 1), '(',float(np.count_nonzero(y_test == 1))/tot*100, '% )')
    ##Class 0:  3712650 ( 99.94928031349289 % )
    ##Class 1:  1884 ( 0.05071968650710
    
    return X_train,y_train,X_test,y_test,feat_names 
   
def PAI(y_pred,y_test,coverage, days, cells):
		
	# Predictive Accuracy Index (PAI) for every day!
	# one day has 10149 cells
	# the testing set contains 366 days
	#print '...calculating the PAI...'
     

	PAI_list = []
	N_list = []
	n_list = []
	alert_list = []
	nN_list = []
	aA_list = []
	hitrate_list = []
	precision_list = []

	for day in range(days):
         #the predicted probability for that day for each cell
         alert_level_daily = y_pred[(day*cells):((day+1)*cells)]
         #the true label of crime/no crime in that day for each cell
         test = y_test[(day*cells):((day+1)*cells)]
         #how many cells do we want to be predicted as hotspots?
         #cells * coverage, eg. 10149 * 0.05 = 507 alerts
         num_samples = round(len(alert_level_daily)*coverage)
         
         #then take num_samples cells with the highest predicted probability
         index_list = np.argsort(alert_level_daily)[-int(num_samples):]
         
         #predicted hotspots -- all zero initially
         pred = np.zeros((alert_level_daily.shape[0]))
         #set to 1 those who are have the top num_samples probability
         pred[index_list] = 1


         n = float(np.sum((test == pred) & (test == 1))) # number of correctly predicted crime areas within all true crimes
         N = float(np.count_nonzero(test)) # total number of crime areas (!= number of crimes, because target is binary)
         a = float(np.count_nonzero(pred)) # total area predicted as hotspot
         #a is fixed by the coverage area, e.g. for 5% = 507
         A = float(cells) # total area 
         #A is always fixed to number of cells = 10149
         
         tp = 0
         for i in range(cells):
             if ((test[i]==pred[i])and (test[i]==1)):
                 tp+=1
      
         N_list.append(N)
         n_list.append(n)
         alert_list.append(np.count_nonzero(pred))
         aA_list.append(a/A)
         
         if ((N == 0) or (a == 0)):
             PAI_list.append(0)
             nN_list.append(0)
             hitrate_list.append(0)
             precision_list.append(0)
         else:
             PAI_list.append((n/N)/(a/A))
             nN_list.append(n/N) #same as hitrate!
             hitrate_list.append(tp/float(N))  # % correctly predicted true crime areas within all true crimes    
             precision_list.append(tp/float(a))# % correctly predicted tru crime areas within all predictived crimes
         #print(day,tp,hitrate,precision,n, N, a, A)
       
     
	return coverage, np.mean(hitrate_list), np.std(hitrate_list), np.mean(precision_list), np.std(precision_list),  np.mean(PAI_list), np.std(PAI_list) 

#%%
def PAI2(y_pred,y_test,coverage, days, cells):
		
	# Predictive Accuracy Index (PAI) for every day!
	# one day has 10149 cells
	# the testing set contains 366 days
	#print '...calculating the PAI...'
     

	PAI_list = []
	N_list = []
	n_list = []
	alert_list = []
	nN_list = []
	aA_list = []
	hitrate_list = []
	precision_list = []

	for day in range(days):
         #the predicted probability for that day for each cell
         alert_level_daily = y_pred[(day*cells):((day+1)*cells)]
         #the true label of crime/no crime in that day for each cell
         test = y_test[(day*cells):((day+1)*cells)]
         #how many cells do we want to be predicted as hotspots?
         #cells * coverage, eg. 10149 * 0.05 = 507 alerts
         num_samples = round(len(alert_level_daily)*coverage)
         
         #predicted hotspots -- all zero initially
         pred = np.zeros((alert_level_daily.shape[0]))
         
         ###
         sorted_alert_levels = sorted(list(set(alert_level_daily)),reverse=True) 
         
         alert_level_to_no_cells = {}
         for a in sorted_alert_levels:
             alert_level_to_no_cells.setdefault(a, 0)
         for i in range(alert_level_daily.shape[0]):
             alert_level_to_no_cells[alert_level_daily[i]] +=1

         #for a in alert_levels:
         #    alert_level_to_cells.setdefault(a, set())
         #    for i in range(len(alert_level_daily)):
         #        if (alert_level_daily[i]==a):
         #            alert_level_to_cells[a].add(i)

         
         index_list = []
         cummulative_samples = 0
         cummulative_samples_new = 0
         threshold = 0
         
         #for a in sorted_alert_levels:
         #    cummulative_samples_new += cummulative_samples + alert_level_to_no_cells[a]
         #    if ((cummulative_samples<=num_samples) and (num_samples<cummulative_samples_new)):
         #        threshold = a 
         #    cummulative_samples = cummulative_samples_new
                 # everything that is higher belongs to the index
                 # everything that is equal and smaller is not in the index
                 
         for a in sorted_alert_levels:
             if ((cummulative_samples<=num_samples) and (num_samples<cummulative_samples + 1)):
                 threshold = a 
             cummulative_samples = cummulative_samples + alert_level_to_no_cells[a]
                 # everything that is higher belongs to the index
                 # everything that is equal and smaller is not in the index
                        
         for i in range(alert_level_daily.shape[0]): 
                 #try also with equal
                 if (alert_level_daily[i] > threshold):
                     index_list.append(i)
         ###           
         #set to 1 those who are have the top num_samples probability
         pred[index_list] = 1
          
         n = float(np.sum((test == pred) & (test == 1))) # number of correctly predicted crime areas within all true crimes
         N = float(np.count_nonzero(test)) # total number of crime areas (!= number of crimes, because target is binary)
         a = float(np.count_nonzero(pred)) # total area predicted as hotspot
         #a is fixed by the coverage area, e.g. for 5% = 507
         A = float(cells) # total area 
         #A is always fixed to number of cells = 10149
         
         tp = 0
         for i in range(cells):
             if ((test[i]==pred[i])and (test[i]==1)):
                 tp+=1
      
         N_list.append(N)
         n_list.append(n)
         alert_list.append(np.count_nonzero(pred))
         aA_list.append(a/A)
         
         if ((N == 0) or (a == 0)):
             PAI_list.append(0)
             nN_list.append(0)
             hitrate_list.append(0)
             precision_list.append(0)
         else:
             PAI_list.append((n/N)/(a/A))
             nN_list.append(n/N) #same as hitrate!
             hitrate_list.append(tp/float(N))  # % correctly predicted true crime areas within all true crimes    
             precision_list.append(tp/float(a))# % correctly predicted tru crime areas within all predictived crimes
         #print(day,tp,hitrate,precision,n, N, a, A)
       
     
	return np.mean(aA_list), np.mean(hitrate_list), np.std(hitrate_list), np.mean(precision_list), np.std(precision_list),  np.mean(PAI_list), np.std(PAI_list) 
# 
#%%
#def main():
    
    #read in data
    _, _, _, y_test, all_features = load_data()
    
    temporal_features = ['dow', 'holiday', 'event', 'temp', 'hum', 'discomf', 'daylight', 'moon']
    
    prior_features = ['prior1d', 'prior3d', 'prior7d', 'prior14d']

    poi_infra_features = ['buildgs_areafrac', 'buildgs_dens', 'land_swim', 'land_oldtown',
       'land_green', 'land_business', 'land_indust', 'land_central',
       'land_public', 'land_park', 'land_special', 'land_river', 'land_resi1',
       'land_resi2', 'land_resi3', 'land_mix2', 'land_mix3', 'land_no_use',
       'land_dividx', 'poi_infra', 'poi_shop', 'poi_gastro', 'poi_public',
       'poi_edu', 'intersection', 'highway_exit', 'border_cross', 'road_type',
       'pub_trans', 'pub_hous']
       
    demographic_features = [ 'popage_1', 'popage_2',
       'popage_3', 'popage_4', 'popage_dividx', 'popbirth_nonCH', 'popcit_CH',
       'popcit_EU', 'popcit_dividx', 'popcit_europ', 'popcit_noneurop',
       'popdens', 'popmale', 'popstab', 'busi_sec1', 'busi_sec2', 'busi_sec3',
       'busidens', 'busisec_dividx', 'empldens', 'emplmale', 'emplsec_dividx']

    #%%
    print(y_test[y_test>1])  
    print(len(y_test))   
    #3714534 cubes = 366 days * 10149 cells
    #print(X_test.shape)
    #%%
    
    ensemble_probas  = np.load(ensemble_probas_path)
    
    #%%
    print(ensemble_probas.shape)
    no_samples = ensemble_probas.shape[0]
    no_models = ensemble_probas.shape[1]
    no_instances = ensemble_probas.shape[2]

#%%
    all_avg = np.zeros(no_instances)
    for i in range(no_instances):
        all_avg[i] = np.mean(ensemble_probas[:, :, i, 1])
    print(all_avg.shape)
#%%
    stacking_avg = np.zeros(no_instances) 
    for i in range(no_instances):
        sample_avg = np.zeros(no_samples)
        for s in range(no_samples):
            sample_avg[s] = np.mean(ensemble_probas[s, :, i, 1])#over all models in the sample
        stacking_avg[i] = np.mean(sample_avg)
    print(stacking_avg.shape)     
#%%
    ensemble_avg = np.zeros(no_instances) 
    for i in range(no_instances):
        models_avg = np.zeros(no_models)
        for m in range(no_models):
            models_avg[m] = np.mean(ensemble_probas[:, m, i, 1])#over all samples results of that model
        ensemble_avg[i] = np.mean(models_avg)
    print(ensemble_avg.shape)         
    #%%
    all_avg_score = roc_auc_score(y_test, all_avg, average='weighted')
    print ('All averaging AUC score (test set): ', all_avg_score)
    #0.797535840124
    stacking_avg_score = roc_auc_score(y_test, stacking_avg, average='weighted')
    print ('First stack in sample, then over samples averaging AUC score (test set): ', stacking_avg_score)
    #0.797535840124
    ensemble_avg_score = roc_auc_score(y_test, ensemble_avg, average='weighted')
    print ('First average over samples, then stack the models averaging AUC score (test set): ', ensemble_avg_score)
    #0.797535840124
    #so they arent better! we stick to ensembling of ensembling
    
    #%%
    ###########################only for urb 0,1,2
    data = only_load_data()
    #%%
    print(data.shape)
    #11123304
    print(11123304/1096) #10149 cells
    plt.hist(data.popdens)
    #%%
    print(max(data.popdens))
    print(np.mean(data.popdens))
    #%%
    urb_level = 2
    print(data[data['urb_lev']==urb_level].shape)
    #(3712176, 73)
    total_cells_urblevel = data[data['urb_lev']==urb_level].shape[0]
    cells_urblevel = total_cells_urblevel / (1096)
    print(cells_urblevel)
    #%%
    #urblevel: 0 - low, 1 - medium, 2 - high
    X_train0, y_train0, X_test0, y_test0, all_features0 = split_data(data, 0)
    X_train1, y_train1, X_test1, y_test1, all_features1 = split_data(data, 1)
    X_train2, y_train2, X_test2, y_test2, all_features2 = split_data(data, 2)
    #%%
    #class imbalance
    print(sum(y_test))
    print(sum(y_test0))
    print(sum(y_test1))
    print(sum(y_test2))
    #1884 
    #151
    #398
    #1335
    #%%
    
    
    #0:
#Total Number of cells:  3361
#Total Number of days:  1096
#Number of training days:  730
#Number of testing days:  366

    #1
#Total Number of cells:  3444

    #2
#Total Number of cells:  3344

# 3361 + 3444 +  3344 = 10149 correct 

    #%%
    #!!! population density changes every year!
    # hence the division below arent integers in the training set
    # but in the test set they should be correct 
    # (the test set covers only 1 year)
    days_train = 730
    days_test = 366
    days_total = 1096
    
    instances_urblevel_train = len(y_train)
    instances_urblevel_test = len(y_test)
    print(instances_urblevel_train)
    print(instances_urblevel_test)
          
    cells_urblevel_train = instances_urblevel_train / days_train
    print(cells_urblevel_train)
    
    cells_urblevel_test = instances_urblevel_test / days_test
    print(cells_urblevel_test)
    #0: 3329 low urb in 2017
    #1: 3393 medium urb in 2017
    #2: 3427 high urb in 2017
    # 3329 + 3393 + 3427 = 10149 also correct  
    
    #cells_urblevel = (instances_urblevel_train + instances_urblevel_test) / days_total
    #print(cells_urblevel)
     #%%
    a2 = np.loadtxt(proba2, delimiter = ",")
    p2 = [row[1] for row in a2] 
    s2 = roc_auc_score(y_test2, p2, average='weighted')
    print ('RF ensemble 2 AUC score (test set): ', s2)
    s2none = roc_auc_score(y_test2, p2, average=None)
    print ('RF ensemble 2 AUC score none (test set): ', s2none)
    s2micro = roc_auc_score(y_test2, p2, average='micro')
    print ('RF ensemble 2 AUC score micro (test set): ', s2micro)
    s2macro = roc_auc_score(y_test2, p2, average='macro')
    print ('RF ensemble 2 AUC score macro (test set): ', s2macro)
    #0.686969043581
    #%%
    a2c = np.loadtxt(proba_onlycrime2, delimiter = ",")
    p2c = [row[1] for row in a2c] 
    s2c = roc_auc_score(y_test, p2c, average='weighted')
    print ('RF ensemble 2 only crime AUC score (test set): ', s2c)
    #0.581392798376
    
    a2s = np.loadtxt(proba_onlyspatial2, delimiter = ",")
    p2s = [row[1] for row in a2s] 
    s2s = roc_auc_score(y_test, p2s, average='weighted')
    print ('RF ensemble 2 only spatial AUC score (test set): ', s2s)
    #0.661250480362

    a2t = np.loadtxt(proba_onlytemporal2, delimiter = ",")
    p2t = [row[1] for row in a2t] 
    s2t = roc_auc_score(y_test, p2t, average='weighted')
    print ('RF ensemble 2 only temporal AUC score (test set): ', s2t)
    #0.554503788908
    
    #%%
    a1 = np.loadtxt(proba1, delimiter = ",")
    p1 = [row[1] for row in a1] 
    s1 = roc_auc_score(y_test, p1, average='weighted')
    print ('RF ensemble 1 AUC score (test set): ', s1)
    #0.725694574537
    
    a1c = np.loadtxt(proba_onlycrime1, delimiter = ",")
    p1c = [row[1] for row in a1c] 
    s1c = roc_auc_score(y_test, p1c, average='weighted')
    print ('RF ensemble 1 only crime AUC score (test set): ', s1c)
    #0.566333533444
    
    a1s = np.loadtxt(proba_onlyspatial1, delimiter = ",")
    p1s = [row[1] for row in a1s] 
    s1s = roc_auc_score(y_test, p1s, average='weighted')
    print ('RF ensemble 1 only spatial AUC score (test set): ', s1s)
    #0.720791963264

    a1t = np.loadtxt(proba_onlytemporal1, delimiter = ",")
    p1t = [row[1] for row in a1t] 
    s1t = roc_auc_score(y_test, p1t, average='weighted')
    print ('RF ensemble 1 only temporal AUC score (test set): ', s1t)
    #0.506690482758
    
    #%%
    a0 = np.loadtxt(proba0, delimiter = ",")
    p0 = [row[1] for row in a0] 
    s0 = roc_auc_score(y_test0, p0, average='weighted')
    print ('RF ensemble 0 AUC score (test set): ', s0)
    s0none = roc_auc_score(y_test0, p0, average=None)
    print ('RF ensemble 2 AUC score none (test set): ', s0none)
    s0micro = roc_auc_score(y_test0, p0, average='micro')
    print ('RF ensemble 2 AUC score micro (test set): ', s0micro)
    s0macro = roc_auc_score(y_test0, p0, average='macro')
    print ('RF ensemble 2 AUC score macro (test set): ', s0macro)
    #0.832677097372
    #%%
    a0c = np.loadtxt(proba_onlycrime0, delimiter = ",")
    p0c = [row[1] for row in a0c] 
    s0c = roc_auc_score(y_test, p0c, average='weighted')
    print ('RF ensemble 0 only crime AUC score (test set): ', s0c)
    #0.546835016371
    
    a0s = np.loadtxt(proba_onlyspatial0, delimiter = ",")
    p0s = [row[1] for row in a0s] 
    s0s = roc_auc_score(y_test, p0s, average='weighted')
    print ('RF ensemble 0 only spatial AUC score (test set): ', s0s)
    #0.833433010227

    a0t = np.loadtxt(proba_onlytemporal0, delimiter = ",")
    p0t = [row[1] for row in a0t] 
    s0t = roc_auc_score(y_test, p0t, average='weighted')
    print ('RF ensemble 0 only temporal AUC score (test set): ', s0t)
    #0.537690045103
    #%%
    # read saved probabilities
    avg_test_proba_file_only_temporal = np.loadtxt(RF_ensemble_proba_path_only_temporal, delimiter = ",")
    
    # compute AUC score
    # second column is the positive class!
    final_pred_proba_only_temporal = [row[1] for row in avg_test_proba_file_only_temporal] 
 
    ## roc_auc_score(y_true, y_score, average=’macro’, sample_weight=None)
    ## y_score = Target scores, can either be probability estimates of the positive class, 
    ##     confidence values, or non-thresholded measure of decisions (as returned by “decision_function” on some classifiers).
    final_score_only_temporal = roc_auc_score(y_test, final_pred_proba_only_temporal, average='weighted')
    print ('RF ensemble only_temporal AUC score (test set): ', final_score_only_temporal)
    #0.539341731544
    #%%
    # read saved probabilities
    avg_test_proba_file_only_spatial = np.loadtxt(RF_ensemble_proba_path_only_spatial, delimiter = ",")
    
    # compute AUC score
    # second column is the positive class!
    final_pred_proba_only_spatial = [row[1] for row in avg_test_proba_file_only_spatial] 
 
    ## roc_auc_score(y_true, y_score, average=’macro’, sample_weight=None)
    ## y_score = Target scores, can either be probability estimates of the positive class, 
    ##     confidence values, or non-thresholded measure of decisions (as returned by “decision_function” on some classifiers).
    final_score_only_spatial = roc_auc_score(y_test, final_pred_proba_only_spatial, average='weighted')
    print ('RF ensemble only_spatial AUC score (test set): ', final_score_only_spatial)
    #0.788442352498
#%%
    # read saved probabilities
    avg_test_proba_file_only_crime = np.loadtxt(RF_ensemble_proba_path_only_crime, delimiter = ",")
    
    # compute AUC score
    # second column is the positive class!
    final_pred_proba_only_crime = [row[1] for row in avg_test_proba_file_only_crime] 
 
    ## roc_auc_score(y_true, y_score, average=’macro’, sample_weight=None)
    ## y_score = Target scores, can either be probability estimates of the positive class, 
    ##     confidence values, or non-thresholded measure of decisions (as returned by “decision_function” on some classifiers).
    final_score_only_crime = roc_auc_score(y_test, final_pred_proba_only_crime, average='weighted')
    print ('RF ensemble only_crime AUC score (test set): ', final_score_only_crime)
    #0.59344173724
    #%%
    # read saved probabilities
    stacking_ensemble_proba_file = np.loadtxt(stacking_ensemble_proba_path, delimiter = ",")
    
    # compute AUC score
    # second column is the positive class!
    stacking_ensemble_pred_proba = [row[1] for row in stacking_ensemble_proba_file] 
 
    ## roc_auc_score(y_true, y_score, average=’macro’, sample_weight=None)
    ## y_score = Target scores, can either be probability estimates of the positive class, 
    ##     confidence values, or non-thresholded measure of decisions (as returned by “decision_function” on some classifiers).
    stacking_ensemble_score = roc_auc_score(y_test, stacking_ensemble_pred_proba, average='weighted')
    print ('Stacking ensemble AUC score (test set): ', stacking_ensemble_score)
    #0.797487934105
    
    #%%
    # read saved probabilities
    lr_stack_test_proba_file = np.loadtxt(LR_stacking_proba_path, delimiter = ",")
    
    # compute AUC score
    # second column is the positive class!
    lr_stack_pred_proba = [row[1] for row in lr_stack_test_proba_file] 
 
    ## roc_auc_score(y_true, y_score, average=’macro’, sample_weight=None)
    ## y_score = Target scores, can either be probability estimates of the positive class, 
    ##     confidence values, or non-thresholded measure of decisions (as returned by “decision_function” on some classifiers).
    lr_stack_score = roc_auc_score(y_test, lr_stack_pred_proba, average='weighted')
    print ('LR stack AUC score (test set): ', lr_stack_score)
    #TODO: insert here

    ####################################################################%%
     #%%
    RF_normal_avg_test_proba_file = np.loadtxt(RF_normal_proba_path, delimiter = ",")
    RF_normal_final_pred_proba = [row[1] for row in RF_normal_avg_test_proba_file] 
    RF_normal_final_score = roc_auc_score(y_test, RF_normal_final_pred_proba, average='weighted')
    print ('RF_normal AUC score (test set): ', RF_normal_final_score)
    #0.76843526106
    #%%
    RF_normal_simple_avg_test_proba_file = np.loadtxt(RF_normal_simple_proba_path, delimiter = ",")
    RF_normal_simple_final_pred_proba = [row[1] for row in RF_normal_simple_avg_test_proba_file] 
    RF_normal_simple_final_score = roc_auc_score(y_test, RF_normal_simple_final_pred_proba, average='weighted')
    print ('RF_normal_simple AUC score (test set): ', RF_normal_simple_final_score)
    #0.537972559416
    ####################################################################%%
    #%%
    AB_avg_test_proba_file = np.loadtxt(AB_ensemble_proba_path, delimiter = ",")
    AB_final_pred_proba = [row[1] for row in AB_avg_test_proba_file] 
    AB_final_score = roc_auc_score(y_test, AB_final_pred_proba, average='weighted')
    print ('AB ensemble AUC score (test set): ', AB_final_score)
    #0.789921004929
    #%%
    LR_avg_test_proba_file = np.loadtxt(LR_ensemble_proba_path, delimiter = ",")
    LR_final_pred_proba = [row[1] for row in LR_avg_test_proba_file] 
    LR_final_score = roc_auc_score(y_test, LR_final_pred_proba, average='weighted')
    print ('LR ensemble AUC score (test set): ', LR_final_score)
    #0.789043750489
    #%%
    LRl1_avg_test_proba_file = np.loadtxt(LRl1_ensemble_proba_path, delimiter = ",")
    LRl1_final_pred_proba = [row[1] for row in LRl1_avg_test_proba_file] 
    LRl1_final_score = roc_auc_score(y_test, LRl1_final_pred_proba, average='weighted')
    print ('LR ensemble AUC score (test set): ', LRl1_final_score)
    #0.775255487543
    #%%
    avg_test_proba_file = np.loadtxt(RF_ensemble_proba_path, delimiter = ",")
    final_pred_proba = [row[1] for row in avg_test_proba_file] 
    final_score = roc_auc_score(y_test, final_pred_proba, average='weighted')
    print ('RF ensemble AUC score (test set): ', final_score)
    #0.79824245994 
   #%%
    RF_costsensitive_test_proba_file = np.loadtxt(RF_costsensitive_proba_path, delimiter = ",")
    RF_costsensitive_pred_proba = [row[1] for row in RF_costsensitive_test_proba_file] 
    RF_costsensitive_score = roc_auc_score(y_test, RF_costsensitive_pred_proba, average='weighted')
    print('RF costsensitive AUC score (test set): ', RF_costsensitive_score)
    #0.781640374864 old
    #0.78625488278
    #%%
    RF_oversampling_test_proba_file = np.loadtxt(RF_oversampling_proba_path, delimiter = ",")
    RF_oversampling_pred_proba = [row[1] for row in RF_oversampling_test_proba_file] 
    RF_oversampling_score = roc_auc_score(y_test, RF_oversampling_pred_proba, average='weighted')
    print ('RF oversampling AUC score (test set): ', RF_oversampling_score)
    #0.525834104139 old
    #0.602827093377
     #%%
    RF_heuristic_oversampling_test_proba_file = np.loadtxt(RF_heuristic_oversampling_proba_path, delimiter = ",")
    RF_heuristic_oversampling_pred_proba = [row[1] for row in RF_heuristic_oversampling_test_proba_file] 
    RF_heuristic_oversampling_score = roc_auc_score(y_test, RF_heuristic_oversampling_pred_proba, average='weighted')
    print ('RF heuristic oversampling AUC score (test set): ', RF_heuristic_oversampling_score)
    #0.530708135121 old
    #0.609207875336
    #%%
    RF_undersampling_test_proba_file = np.loadtxt(RF_undersampling_proba_path, delimiter = ",")
    RF_undersampling_pred_proba = [row[1] for row in RF_undersampling_test_proba_file] 
    RF_undersampling_score = roc_auc_score(y_test, RF_undersampling_pred_proba, average='weighted')
    print ('RF undersampling AUC score (test set): ', RF_undersampling_score)
    # 0.790412282698
    #%%
    RF_heuristic_undersampling_test_proba_file = np.loadtxt(RF_heuristic_undersampling_proba_path, delimiter = ",")
    RF_heuristic_undersampling_pred_proba = [row[1] for row in RF_heuristic_undersampling_test_proba_file] 
    RF_heuristic_undersampling_score = roc_auc_score(y_test, RF_heuristic_undersampling_pred_proba, average='weighted')
    print ('RF heuristic undersampling AUC score (test set): ', RF_heuristic_undersampling_score)
    # 0.728841539225
    #%%       
    # majority class dummy classifier
    y_test_maj_class = np.zeros_like(y_test)
    print(y_test_maj_class)
    final_score_maj_class = roc_auc_score(y_test, y_test_maj_class, average='weighted')
    print ('Majority Class AUC score (test set): ', final_score_maj_class)
    #0.5
    #%%
    
    # read saved probabilities
    AB_undersampling_test_proba_file = np.loadtxt(AB_undersampling_proba_path, delimiter = ",")
    
    # compute AUC score
    # second column is the positive class!
    AB_undersampling_pred_proba = [row[1] for row in AB_undersampling_test_proba_file] 
 
    ## roc_auc_score(y_true, y_score, average=’macro’, sample_weight=None)
    ## y_score = Target scores, can either be probability estimates of the positive class, 
    ##     confidence values, or non-thresholded measure of decisions (as returned by “decision_function” on some classifiers).
    AB_undersampling_score = roc_auc_score(y_test, AB_undersampling_pred_proba, average='weighted')
    print ('AB undersampling AUC score (test set): ', AB_undersampling_score)
    
    
    # read saved probabilities
    LR_undersampling_test_proba_file = np.loadtxt(LR_undersampling_proba_path, delimiter = ",")
    
    # compute AUC score
    # second column is the positive class!
    LR_undersampling_pred_proba = [row[1] for row in LR_undersampling_test_proba_file] 
 
    ## roc_auc_score(y_true, y_score, average=’macro’, sample_weight=None)
    ## y_score = Target scores, can either be probability estimates of the positive class, 
    ##     confidence values, or non-thresholded measure of decisions (as returned by “decision_function” on some classifiers).
    LR_undersampling_score = roc_auc_score(y_test, LR_undersampling_pred_proba, average='weighted')
    print ('LR undersampling AUC score (test set): ', LR_undersampling_score)
    
    
    # read saved probabilities
    LRl1_undersampling_test_proba_file = np.loadtxt(LRl1_undersampling_proba_path, delimiter = ",")
    
    # compute AUC score
    # second column is the positive class!
    LRl1_undersampling_pred_proba = [row[1] for row in LRl1_undersampling_test_proba_file] 
 
    ## roc_auc_score(y_true, y_score, average=’macro’, sample_weight=None)
    ## y_score = Target scores, can either be probability estimates of the positive class, 
    ##     confidence values, or non-thresholded measure of decisions (as returned by “decision_function” on some classifiers).
    LRl1_undersampling_score = roc_auc_score(y_test, LRl1_undersampling_pred_proba, average='weighted')
    print ('LR l1 undersampling AUC score (test set): ', LRl1_undersampling_score)
    
    
    #%%
    #stacking by simple average
    test_probas = []

    test_probas.append(RF_undersampling_pred_proba)
    test_probas.append(AB_undersampling_pred_proba)
    test_probas.append(LR_undersampling_pred_proba)
    test_probas.append(LRl1_undersampling_pred_proba)
    
    # compute averages of all models
    avg_stacking_test_proba = np.mean( np.array( test_probas ), axis=0)
    
    score_avg_stacking = roc_auc_score(y_test, avg_stacking_test_proba, average='weighted')
    print ('Average stack AUC score (test set): ', score_avg_stacking)
    
    #%%
    # print ROC curve
    fig, axs = plt.subplots(1, 1)
    

    ROC_curve(final_pred_proba, y_test, "RF ensemble ", "black", '-')
    ROC_curve(AB_final_pred_proba, y_test, "AB ensemble ", "black", '--')
    ROC_curve(LR_final_pred_proba, y_test, "LR L2 ensemble ", "black", '-.')
    ROC_curve(LRl1_final_pred_proba, y_test, "LR L1 ensemble ", "black", '-.')
    ROC_curve(y_test_maj_class, y_test, 'Majority class ', 'grey', ':')
    
    plt.xlim([0,1])
    plt.ylim([-0,1])
    plt.grid(color='grey', linestyle='-', linewidth=2, alpha=0.1)

    #axs.set_aspect('equal', 'box')
    plt.tight_layout()
    plt.yticks(np.arange(0, 1.1, step=0.1))
    plt.xticks(np.arange(0, 1.1, step=0.1))
    plt.show()   
    #%%
    # print ROC curve
    fig, axs = plt.subplots(1, 1)
    
    #ROC_curve(stacking_ensemble_pred_proba, y_test, "stacking ensemble", "black", '-')
    #####ROC_curve(final_pred_proba, y_test, "RF ensemble ", "black", '-')
    #ROC_curve(avg_stacking_test_proba, y_test, "avg stacking undersampling ", "black", '--')
    #ROC_curve(lr_stack_pred_proba, y_test, "LR stacking undersampling ", "black", '-')
     
    #####ROC_curve(RF_undersampling_pred_proba, y_test, 'RF undersampling ', 'black', '--')
    
    #ROC_curve(AB_undersampling_pred_proba, y_test, 'AB undersampling ', 'black', ':')
    #ROC_curve(LR_undersampling_pred_proba, y_test, 'LR l2 undersampling ', 'black', '-.')
    #ROC_curve(LRl1_undersampling_pred_proba, y_test, 'LR l1 undersampling ', 'black', ':')
    
    #####ROC_curve(RF_heuristic_undersampling_pred_proba, y_test, 'RF heuristic undersampling ', 'black', '-.')
   
    #####ROC_curve(RF_oversampling_pred_proba, y_test, 'RF oversampling ', 'black', '-.')
    #####ROC_curve(RF_heuristic_oversampling_pred_proba, y_test, 'RF heuristic oversampling ', 'black', '-.')
    #####ROC_curve(RF_costsensitive_pred_proba, y_test, 'RF costsensitive ', 'black', '-.')
    ROC_curve(RF_normal_final_pred_proba, y_test, 'Naive ', 'grey', ':')
    #ROC_curve(RF_normal_simple_final_pred_proba, y_test, 'Naive ', 'grey', ':')
    #####ROC_curve(y_test_maj_class, y_test, 'Majority class ', 'grey', ':')
    plt.xlim([0,1])
    plt.ylim([-0,1])
    plt.grid(color='grey', linestyle='-', linewidth=2, alpha=0.1)

    #axs.set_aspect('equal', 'box')
    plt.tight_layout()
    plt.yticks(np.arange(0, 1.1, step=0.1))
    plt.xticks(np.arange(0, 1.1, step=0.1))
    plt.show()

#%%
    # print ROC curve
    fig, axs = plt.subplots(1, 1)
    
    ROC_curve(final_pred_proba, y_test, "All ", "black", '-')
    ROC_curve(final_pred_proba_only_crime, y_test, 'Crime only ', 'black', '-.')
    ROC_curve(final_pred_proba_only_spatial, y_test, 'Spatial only ', 'black', '--')
    ROC_curve(final_pred_proba_only_temporal, y_test, 'Temporal only ', 'black', ':')
    ROC_curve(y_test_maj_class, y_test, 'Majority class ', 'grey', ':') 
    #TODO: insert here
        
    plt.xlim([0,1])
    plt.ylim([-0,1])
    plt.grid(color='grey', linestyle='-', linewidth=2, alpha=0.1)

    #axs.set_aspect('equal', 'box')
    plt.tight_layout()
    plt.yticks(np.arange(0, 1.1, step=0.1))
    plt.xticks(np.arange(0, 1.1, step=0.1))
    plt.show()

    #%%
    # print ROC curve
    fig, axs = plt.subplots(1, 1)
    
    ROC_curve(final_pred_proba, y_test, "All ", "black", '-')
    
    ROC_curve(p0, y_test0, 'Low Urbanization ', 'black', '--')
    ROC_curve(p1, y_test1, 'Medium Urbanization ', 'black', '-.')
    ROC_curve(p2, y_test2, 'High Urbanization ', 'black', ':')

    ROC_curve(y_test_maj_class, y_test, 'Majority class ', 'grey', ':')    
    plt.xlim([0,1])
    plt.ylim([-0,1])
    plt.grid(color='grey', linestyle='-', linewidth=2, alpha=0.1)

    #axs.set_aspect('equal', 'box')
    plt.tight_layout()
    plt.yticks(np.arange(0, 1.1, step=0.1))
    plt.xticks(np.arange(0, 1.1, step=0.1))
    plt.show()
    #%%
    #compute coverage area, hitrate, precision, PAI
    #and save it
    
    days = 366
    cells = 10149
    pred_cov_list = np.arange(0.05,1.05,0.05)

    #%%
    # for subregions
    days = 366
    cells2 = 3427 
    cells1 = 3393 
    cells0 = 3329
    pred_cov_list = np.arange(0.05,1.05,0.05)
    #0: 3329 low urb in 2017
    #1: 3393 medium urb in 2017
    #2: 3427 high urb in 2017
    # 3329 + 3393 + 3427 = 10149 also correct  
    #%%
        #%%
    RF0s = []
    for pred_cov in tqdm(pred_cov_list):

        print('\n')
        print('coverage: ', pred_cov)
        coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std = PAI(np.array(p0),y_test0,pred_cov, days, cells0)
        
        
        print('Hitrate: ', hitrate_mean, hitrate_std)
        print('Precision: ', precision_mean, precision_std)
        print('PAI: ', PAI_mean, PAI_std)
        
        RF0s.append([coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std])
 
    
    np.savetxt(path0s, RF0s, delimiter=",")

#coverage:  0.05
#Hitrate:  0.104508196721 0.283413978279
#Precision:  0.000905260385806 0.00245397870374
#PAI:  2.09583004148 5.68364538368

#coverage:  0.1
#Hitrate:  0.156876138434 0.345957114795
#Precision:  0.000648189172779 0.00147206986929
#PAI:  1.56829028482 3.4585322377

#coverage:  0.2
#Hitrate:  0.212204007286 0.399669489633
#Precision:  0.000443066016837 0.000902168857868
#PAI:  1.06070141179 1.99774734382
        #%%
    RF0t = []
    for pred_cov in tqdm(pred_cov_list):

        print('\n')
        print('coverage: ', pred_cov)
        coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std = PAI(np.array(p0t),y_test0,pred_cov, days, cells0)
        
        
        print('Hitrate: ', hitrate_mean, hitrate_std)
        print('Precision: ', precision_mean, precision_std)
        print('PAI: ', PAI_mean, PAI_std)
        
        RF0t.append([coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std])
 
    
    np.savetxt(path0t, RF0t, delimiter=",")

#coverage:  0.05
#Hitrate:  0.00705828779599 0.0672524239507
#Precision:  8.22963987096e-05 0.000699277298909
#PAI:  0.141548434174 1.34869469477

#coverage:  0.1
#Hitrate:  0.0384790528233 0.18017673309
#Precision:  0.000147688672279 0.000649382462377
#PAI:  0.384674975522 1.80122625963

#coverage:  0.2
#Hitrate:  0.0648907103825 0.231440481802
#Precision:  0.000131278819803 0.00045223957661
#PAI:  0.324356118413 1.15685490078
        #%%
    RF0c = []
    for pred_cov in tqdm(pred_cov_list):

        print('\n')
        print('coverage: ', pred_cov)
        coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std = PAI(np.array(p0c),y_test0,pred_cov, days, cells0)
        
        
        print('Hitrate: ', hitrate_mean, hitrate_std)
        print('Precision: ', precision_mean, precision_std)
        print('PAI: ', PAI_mean, PAI_std)
        
        RF0c.append([coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std])
 
    
    np.savetxt(path0c, RF0c, delimiter=",")

#coverage:  0.05
#Hitrate:  0.0405282331512 0.186716979359
#Precision:  0.000312726315096 0.0014086874357
#PAI:  0.812761976869 3.7444627969

#coverage:  0.1
#Hitrate:  0.0507741347905 0.207312087799
#Precision:  0.000196918229705 0.000775788684
#PAI:  0.507588873026 2.07249831917

#coverage:  0.2
#Hitrate:  0.0803734061931 0.256981237878
#Precision:  0.000155893598517 0.000484160176305
#PAI:  0.401746350175 1.28452033167
    #%%
    RF0 = []
    for pred_cov in tqdm(pred_cov_list):

        print('\n')
        print('coverage: ', pred_cov)
        coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std = PAI(np.array(p0),y_test0,pred_cov, days, cells0)
        
        
        print('Hitrate: ', hitrate_mean, hitrate_std)
        print('Precision: ', precision_mean, precision_std)
        print('PAI: ', PAI_mean, PAI_std)
        
        RF0.append([coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std])
 
    
    np.savetxt(path0, RF0, delimiter=",")

#coverage:  0.05
#Hitrate:  0.104508196721 0.283413978279
#Precision:  0.000905260385806 0.00245397870374
#PAI:  2.09583004148 5.68364538368    

#coverage:  0.1
#Hitrate:  0.156876138434 0.345957114795
#Precision:  0.000648189172779 0.00147206986929
#PAI:  1.56829028482 3.4585322377

#coverage:  0.2
#Hitrate:  0.212204007286 0.399669489633
#Precision:  0.000443066016837 0.000902168857868
#PAI:  1.06070141179 1.99774734382
    #%%
    RF1s = []
    for pred_cov in tqdm(pred_cov_list):

        print('\n')
        print('coverage: ', pred_cov)
        coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std = PAI(np.array(p1s), y_test1, pred_cov, days, cells1)
        
        
        print('Hitrate: ', hitrate_mean, hitrate_std)
        print('Precision: ', precision_mean, precision_std)
        print('PAI: ', PAI_mean, PAI_std)
        
        RF1s.append([coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std])
 
    
    np.savetxt(path1s, RF1s, delimiter=",")
    
#coverage:  0.05
#Hitrate:  0.140534738486 0.308036287759
#Precision:  0.00138219222115 0.00291360184986
#PAI:  2.80490804519 6.14804190803

#coverage:  0.1
#Hitrate:  0.20410486599 0.364104777015
#Precision:  0.00103164240695 0.0018962075664
#PAI:  2.04285489765 3.64426993632

#coverage:  0.2
#Hitrate:  0.314669529014 0.418351959373
#Precision:  0.00080478363392 0.00113325995399
#PAI:  1.57242078342 2.09052753778
    #%%
    RF1t = []
    for pred_cov in tqdm(pred_cov_list):

        print('\n')
        print('coverage: ', pred_cov)
        coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std = PAI(np.array(p1t), y_test1, pred_cov, days, cells1)
        
        
        print('Hitrate: ', hitrate_mean, hitrate_std)
        print('Precision: ', precision_mean, precision_std)
        print('PAI: ', PAI_mean, PAI_std)
        
        RF1t.append([coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std])
 
    
    np.savetxt(path1t, RF1t, delimiter=",")
    
#coverage:  0.05
#Hitrate:  0.0311930783242 0.152874115546
#Precision:  0.000289296046287 0.00127202564689
#PAI:  0.622577145612 3.0511874944

#coverage:  0.1
#Hitrate:  0.0762750455373 0.232230280324
#Precision:  0.000354627077389 0.000983812532499
#PAI:  0.763425455776 2.32435793846

#coverage:  0.2
#Hitrate:  0.145218579235 0.319457167382
#Precision:  0.000342033044416 0.000759199371019
#PAI:  0.725665153673 1.59634487324
    #%%
    RF1c = []
    for pred_cov in tqdm(pred_cov_list):

        print('\n')
        print('coverage: ', pred_cov)
        coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std = PAI(np.array(p1c), y_test1, pred_cov, days, cells1)
        
        
        print('Hitrate: ', hitrate_mean, hitrate_std)
        print('Precision: ', precision_mean, precision_std)
        print('PAI: ', PAI_mean, PAI_std)
        
        RF1c.append([coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std])
 
    
    np.savetxt(path1c, RF1c, delimiter=",")
    
#coverage:  0.05
#Hitrate:  0.0844782721832 0.242612028463
#Precision:  0.000932176149148 0.0025505412311
#PAI:  1.68608692657 4.84225066221

#coverage:  0.1
#Hitrate:  0.135597189696 0.301283737826
#Precision:  0.000733433273692 0.00163453148879
#PAI:  1.35717187209 3.01550360603

#coverage:  0.2
#Hitrate:  0.203903200625 0.359562330961
#Precision:  0.000531157198387 0.000956542142539
#PAI:  1.01891540459 1.79675256105    
    #%%
    RF1 = []
    for pred_cov in tqdm(pred_cov_list):

        print('\n')
        print('coverage: ', pred_cov)
        coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std = PAI(np.array(p1), y_test1, pred_cov, days, cells1)
        
        
        print('Hitrate: ', hitrate_mean, hitrate_std)
        print('Precision: ', precision_mean, precision_std)
        print('PAI: ', PAI_mean, PAI_std)
        
        RF1.append([coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std])
 
    
    np.savetxt(path1, RF1, delimiter=",")

#coverage:  0.05
#Hitrate:  0.156453291699 0.32783198889
#Precision:  0.00160720025715 0.00348788377423
#PAI:  3.12262363962 6.54314081356

#coverage:  0.1
#Hitrate:  0.215378610461 0.370048228537
#Precision:  0.00111223946999 0.00198641922534
#PAI:  2.15569211001 3.70375704845

#coverage:  0.2
#Hitrate:  0.298848555816 0.410068673535
#Precision:  0.000792711879411 0.00116993647545
#PAI:  1.49336251824 2.04913550708
#%%
    RF2s = []
    for pred_cov in tqdm(pred_cov_list):

        print('\n')
        print('coverage: ', pred_cov)
        coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std = PAI(np.array(p2s), y_test2, pred_cov, days, cells2)
        
        
        print('Hitrate: ', hitrate_mean, hitrate_std)
        print('Precision: ', precision_mean, precision_std)
        print('PAI: ', PAI_mean, PAI_std)
        
        RF2.append([coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std])
 
    
    np.savetxt(path2s, RF2s, delimiter=",")

#coverage:  0.05
#Hitrate:  0.151338472547 0.226877712093
#Precision:  0.00329147093599 0.00451383851545
#PAI:  3.0329645931 4.54684163359

#coverage:  0.1
#Hitrate:  0.23498839882 0.257537918022
#Precision:  0.00260478898819 0.00270404072934
#PAI:  2.34782869609 2.57312666199

#coverage:  0.2
#Hitrate:  0.39172250412 0.303340779288
#Precision:  0.00212596226716 0.0017613725207
#PAI:  1.95975623594 1.51758956295
    #%%
    RF2t = []
    for pred_cov in tqdm(pred_cov_list):

        print('\n')
        print('coverage: ', pred_cov)
        coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std = PAI(np.array(p2t), y_test2, pred_cov, days, cells2)
        
        
        print('Hitrate: ', hitrate_mean, hitrate_std)
        print('Precision: ', precision_mean, precision_std)
        print('PAI: ', PAI_mean, PAI_std)
        
        RF2t.append([coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std])
 
    
    np.savetxt(path2t, RF2t, delimiter=",")
    
#coverage:  0.05
#Hitrate:  0.0598062494579 0.15226114083
#Precision:  0.00127824114019 0.00270854441574
#PAI:  1.1985731982 3.0514557288

#coverage:  0.1
#Hitrate:  0.113329430133 0.214493252938
#Precision:  0.00121078876515 0.00208324403216
#PAI:  1.13230308182 2.1430564951

#coverage:  0.2
#Hitrate:  0.216545233758 0.261335325075
#Precision:  0.00121255633999 0.00144279706204
#PAI:  1.08335841765 1.30743964822
    #%%
    RF2c = []
    for pred_cov in tqdm(pred_cov_list):

        print('\n')
        print('coverage: ', pred_cov)
        coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std = PAI(np.array(p2c), y_test2, pred_cov, days, cells2)
        
        
        print('Hitrate: ', hitrate_mean, hitrate_std)
        print('Precision: ', precision_mean, precision_std)
        print('PAI: ', PAI_mean, PAI_std)
        
        RF2c.append([coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std])
 
    
    np.savetxt(path2c, RF2c, delimiter=",")

#coverage:  0.05
#Hitrate:  0.115742367074 0.204176893375
#Precision:  0.00266832838015 0.00435309261682
#PAI:  2.31958533312 4.09189598594

#coverage:  0.1
#Hitrate:  0.218642661983 0.266512622335
#Precision:  0.00236581752139 0.00286722609296
#PAI:  2.18451429334 2.66279520916

#coverage:  0.2
#Hitrate:  0.350891230809 0.301203003676
#Precision:  0.00194248334729 0.00183297171847
#PAI:  1.75548065399 1.50689444321
#%%
    RF2 = []
    for pred_cov in tqdm(pred_cov_list):

        print('\n')
        print('coverage: ', pred_cov)
        coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std = PAI(np.array(p2), y_test2, pred_cov, days, cells2)
        
        
        print('Hitrate: ', hitrate_mean, hitrate_std)
        print('Precision: ', precision_mean, precision_std)
        print('PAI: ', PAI_mean, PAI_std)
        
        RF2.append([coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std])
 
    
    np.savetxt(path2, RF2, delimiter=",")

#coverage:  0.05
#Hitrate:  0.150329603608 0.222558668437
#Precision:  0.00329147093599 0.00443026282571
#PAI:  3.01274591559 4.46028395751

#coverage:  0.1
#Hitrate:  0.272342570908 0.280779648083
#Precision:  0.00287562331724 0.0029350284052
#PAI:  2.72104370409 2.80534068216

#coverage:  0.2
#Hitrate:  0.418002970769 0.307052913625
#Precision:  0.002285509154 0.001909107841
#PAI:  2.09123530048 1.53616107298
    #%%DONE urblevel
    
    #%%
    RF_ensemble_cov_list_only_temporal = []
    for pred_cov in tqdm(pred_cov_list):

        print('\n')
        print('coverage: ', pred_cov)
        coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std = PAI(np.array(final_pred_proba_only_temporal),y_test,pred_cov, days, cells)
        
        
        print('Hitrate: ', hitrate_mean, hitrate_std)
        print('Precision: ', precision_mean, precision_std)
        print('PAI: ', PAI_mean, PAI_std)
        
        RF_ensemble_cov_list_only_temporal.append([coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std])
 
    
    np.savetxt(RF_ensemble_coverage_path_only_temporal, RF_ensemble_cov_list_only_temporal, delimiter=",")
#coverage:  0.05
#Hitrate:  0.064529903157 0.132884275943
#Precision:  0.000695185436673 0.00121783831142
#PAI:  1.29174356438 2.66004441132    

#coverage:  0.1
#Hitrate:  0.125483616262 0.189839664346
#Precision:  0.000662198174917 0.000967094452684
#PAI:  1.25471253344 1.89820960931

#coverage:  0.2
#Hitrate:  0.21143687824 0.236925039593
#Precision:  0.000553177743681 0.000634636199801
#PAI:  1.0570802351 1.18450848612
    #%%
    RF_ensemble_cov_list_only_spatial = []
    for pred_cov in tqdm(pred_cov_list):

        print('\n')
        print('coverage: ', pred_cov)
        coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std = PAI(np.array(final_pred_proba_only_spatial),y_test,pred_cov, days, cells)
        
        
        print('Hitrate: ', hitrate_mean, hitrate_std)
        print('Precision: ', precision_mean, precision_std)
        print('PAI: ', PAI_mean, PAI_std)
        
        RF_ensemble_cov_list_only_spatial.append([coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std])
 
    
    np.savetxt(RF_ensemble_coverage_path_only_spatial, RF_ensemble_cov_list_only_spatial, delimiter=",")
#coverage:  0.05
#Hitrate:  0.232534378948 0.225692284877
#Precision:  0.00236039706405 0.00229109395754
#PAI:  4.65481540818 4.51785206945  

#coverage:  0.1
#Hitrate:  0.371098027655 0.256720886919
#Precision:  0.00189237933726 0.0014517626495
#PAI:  3.71061466273 2.56695594221

#coverage:  0.2
#Hitrate:  0.572071644203 0.263785227916
#Precision:  0.00148456216856 0.000956169301517
#PAI:  2.86007641232 1.31879619612
    #%%
    RF_ensemble_cov_list_only_crime = []
    for pred_cov in tqdm(pred_cov_list):

        print('\n')
        print('coverage: ', pred_cov)
        coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std = PAI(np.array(final_pred_proba_only_crime),y_test,pred_cov, days, cells)
        
        
        print('Hitrate: ', hitrate_mean, hitrate_std)
        print('Precision: ', precision_mean, precision_std)
        print('PAI: ', PAI_mean, PAI_std)
        
        RF_ensemble_cov_list_only_crime.append([coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std])
 
    
    np.savetxt(RF_ensemble_coverage_path_only_crime, RF_ensemble_cov_list_only_crime, delimiter=",")

#coverage:  0.05
#Hitrate:  0.160520327426 0.190747355988
#Precision:  0.00170293486813 0.00201284582715
#PAI:  3.21325602179 3.81833316749

#coverage:  0.1
#Hitrate:  0.274360536246 0.236007254519
#Precision:  0.00137284987483 0.00125443103075
#PAI:  2.74333505651 2.35984002573

#coverage:  0.2
#Hitrate:  0.389880069081 0.257989813897
#Precision:  0.00100810250612 0.000777697897532
#PAI:  1.94920828626 1.2898219809
    #%% changed to PAI2 to test
    stacking_ensemble_cov_list = []
    for pred_cov in tqdm(pred_cov_list):

        print('\n')
        print('coverage: ', pred_cov)
        coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std = PAI2(np.array(stacking_ensemble_pred_proba),y_test,pred_cov, days, cells)
        
        
        print('Hitrate: ', hitrate_mean, hitrate_std)
        print('Precision: ', precision_mean, precision_std)
        print('PAI: ', PAI_mean, PAI_std)
        
        stacking_ensemble_cov_list.append([coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std])

    np.savetxt(stacking_ensemble_coverage_path, stacking_ensemble_cov_list, delimiter=",")

#coverage:  0.05
#Hitrate:  0.255662980765 0.227191780288
#Precision:  0.00259751457734 0.0023628073648
#PAI:  5.11779801142 4.54786859595

#coverage:  0.1
#Hitrate:  0.389630117909 0.251224339062
#Precision:  0.00203235618725 0.00153250216845
#PAI:  3.89591730705 2.51199587896

#coverage:  0.2
#Hitrate:  0.600984060718 0.267899830489
#Precision:  0.00155051279981 0.000998601171551
#PAI:  3.00462425233 1.33936718208 
    #%%
    
    RF_normal_cov_list = []
    for pred_cov in tqdm(pred_cov_list):

        print('\n')
        print('coverage: ', pred_cov)
        coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std = PAI2(np.array(RF_normal_final_pred_proba),y_test,pred_cov, days, cells)
        
        
        print('Hitrate: ', hitrate_mean, hitrate_std)
        print('Precision: ', precision_mean, precision_std)
        print('PAI: ', PAI_mean, PAI_std)
        
        RF_normal_cov_list.append([coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std])
 
    
    np.savetxt(RF_normal_coverage_path, RF_normal_cov_list, delimiter=",")  
    
#coverage:  0.05
#Hitrate:  0.180876265507 0.206087469823
#Precision:  0.00181071555599 0.00194946038025
#PAI:  3.62073613143 4.12540775391

#coverage:  0.1
#Hitrate:  0.324930708332 0.255080661594
#Precision:  0.00165280357479 0.00135466760059
#PAI:  3.24898695455 2.55055530494

#coverage:  0.2
#Hitrate:  0.530194547408 0.252183439536
#Precision:  0.00136612021858 0.000878563260172
#PAI:  2.65071155746 1.26079296938

    #%%
    coverages = [v[0] for v in RF_normal_cov_list] 
    hitrates = [v[1] for v in RF_normal_cov_list] 
    #print(coverages)
    auc_RF_normal = auc(coverages, hitrates)
    print(auc_RF_normal)
    #0.753549883892
    #%%
    
    RF_normal_simple_cov_list = []
    for pred_cov in tqdm(pred_cov_list):

        print('\n')
        print('coverage: ', pred_cov)
        coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std = PAI2(np.array(RF_normal_simple_final_pred_proba),y_test,pred_cov, days, cells)
        
        
        print('Hitrate: ', hitrate_mean, hitrate_std)
        print('Precision: ', precision_mean, precision_std)
        print('PAI: ', PAI_mean, PAI_std)
        
        RF_normal_simple_cov_list.append([coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std])
 
    
    np.savetxt(RF_normal_simple_coverage_path, RF_normal_simple_cov_list, delimiter=",")   
#with PAI    
#coverage:  0.05
#Hitrate:  0.127914514902 0.179441947508
#Precision:  0.00126642308231 0.00160420429667
#PAI:  2.56056096991 3.59202431017

#coverage:  0.1
#Hitrate:  0.182451670976 0.195262943421
#Precision:  0.000939460012383 0.000985504794597
#PAI:  1.82433695442 1.95243705693

#coverage:  0.2
#Hitrate:  0.300654119711 0.241031674582
#Precision:  0.000780640124902 0.000662947314508
#PAI:  1.50312249308 1.2050396381

#with PAI2
#coverage:  0.05
#Hitrate:  0.125920087805 0.176910301626
#Precision:  0.00131853322683 0.00170388179401
#PAI:  2.71299173663 4.14489406185
    #%%
    
    LRl1_ensemble_cov_list = []
    for pred_cov in tqdm(pred_cov_list):

        print('\n')
        print('coverage: ', pred_cov)
        coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std = PAI(np.array(LRl1_final_pred_proba),y_test,pred_cov, days, cells)
        
        
        print('Hitrate: ', hitrate_mean, hitrate_std)
        print('Precision: ', precision_mean, precision_std)
        print('PAI: ', PAI_mean, PAI_std)
        
        LRl1_ensemble_cov_list.append([coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std])
 
    
    np.savetxt(LRl1_ensemble_coverage_path, LRl1_ensemble_cov_list, delimiter=",") 

#coverage:  0.05
#Hitrate:  0.22052740065 0.221443438785
#Precision:  0.00223644927302 0.00218811387574
#PAI:  4.41446270059 4.43279972432

#coverage:  0.1
#Hitrate:  0.340935525464 0.252378605497
#Precision:  0.00176047807478 0.00141912989432
#PAI:  3.40901935757 2.5235374061    

#coverage:  0.2
#Hitrate:  0.544539560318 0.259018086402
#Precision:  0.00140515222482 0.000935525820553
#PAI:  2.7224295555 1.29496283689
    #%%
    LR_ensemble_cov_list = []
    for pred_cov in tqdm(pred_cov_list):

        print('\n')
        print('coverage: ', pred_cov)
        coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std = PAI(np.array(LR_final_pred_proba),y_test,pred_cov, days, cells)
        
        
        print('Hitrate: ', hitrate_mean, hitrate_std)
        print('Precision: ', precision_mean, precision_std)
        print('PAI: ', PAI_mean, PAI_std)
        
        LR_ensemble_cov_list.append([coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std])
 
    
    np.savetxt(LR_ensemble_coverage_path, LR_ensemble_cov_list, delimiter=",")   

#coverage:  0.05
#Hitrate:  0.246575544219 0.233687556528
#Precision:  0.00251667906145 0.00241595286315
#PAI:  4.93588796505 4.67789943038

#coverage:  0.1
#Hitrate:  0.373092413051 0.249242640934
#Precision:  0.00192198982476 0.00144665008467
#PAI:  3.73055655178 2.49218085009

#coverage:  0.2
#Hitrate:  0.57367545554 0.273130303562
#Precision:  0.00150609706856 0.00100514033997
#PAI:  2.86809467895 1.36551697086
    #%%
    AB_ensemble_cov_list = []
    for pred_cov in tqdm(pred_cov_list):

        print('\n')
        print('coverage: ', pred_cov)
        coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std = PAI(np.array(AB_final_pred_proba),y_test,pred_cov, days, cells)
        
        
        print('Hitrate: ', hitrate_mean, hitrate_std)
        print('Precision: ', precision_mean, precision_std)
        print('PAI: ', PAI_mean, PAI_std)
        
        AB_ensemble_cov_list.append([coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std])
 
    
    np.savetxt(AB_ensemble_coverage_path, AB_ensemble_cov_list, delimiter=",") 
    
#coverage:  0.05
#Hitrate:  0.232740312966 0.220955121278
#Precision:  0.00234961899527 0.00227427467326
#PAI:  4.65893774416 4.42302470582

#coverage:  0.1
#Hitrate:  0.380921268319 0.255279730376
#Precision:  0.00196505962475 0.00154150440894
#PAI:  3.8088373913 2.55254579664

#coverage:  0.2
#Hitrate:  0.591789441585 0.274652679048
#Precision:  0.00153974534981 0.00102228418112
#PAI:  2.95865568603 1.37312809835

    #%% 
    RF_ensemble_cov_list = []
    for pred_cov in tqdm(pred_cov_list):

        print('\n')
        print('coverage: ', pred_cov)
        coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std = PAI(np.array(final_pred_proba),y_test,pred_cov, days, cells)
        
        
        print('Hitrate: ', hitrate_mean, hitrate_std)
        print('Precision: ', precision_mean, precision_std)
        print('PAI: ', PAI_mean, PAI_std)
        
        RF_ensemble_cov_list.append([coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std])
 
    
    np.savetxt(RF_ensemble_coverage_path, RF_ensemble_cov_list, delimiter=",")

#coverage:  0.05
#Hitrate:  0.246380285622 0.221092014526
#Precision:  0.00253823519902 0.00229813727291
#PAI:  4.93197932698 4.42576500084

#coverage: 0.1
#Hitrate:  0.402007433975 0.256439841721
#Precision:  0.00207273412474 0.00153811619348
#PAI:  4.01967827331 2.5641457671

#coverage:  0.2
#Hitrate:  0.604166825888 0.264823040751
#Precision:  0.00156262618105 0.00100261760535
#PAI:  3.02053651031 1.32398474905
    #%%
    coverages = [v[0] for v in RF_ensemble_cov_list] 
    hitrates = [v[1] for v in RF_ensemble_cov_list] 
    #print(coverages)
    auc_RF_ensemble = auc(coverages, hitrates)
    print(auc_RF_ensemble)
    #0.778971138625
    #%%
    undersampling_cov_list = []
    for pred_cov in tqdm(pred_cov_list):

        undersampling_coverage, undersampling_hitrate_mean, undersampling_hitrate_std, undersampling_precision_mean, undersampling_precision_std, undersampling_PAI_mean, undersampling_PAI_std = PAI(np.array(RF_undersampling_pred_proba),y_test,pred_cov, days, cells)
        
        print('\n')
        print('coverage: ', undersampling_coverage)
        
        print('Hitrate: ', undersampling_hitrate_mean, undersampling_hitrate_std)
        print('Precision: ', undersampling_precision_mean, undersampling_precision_std)
        print('PAI: ', undersampling_PAI_mean, undersampling_PAI_std)
        
        undersampling_cov_list.append([undersampling_coverage, undersampling_hitrate_mean, undersampling_hitrate_std, undersampling_precision_mean, undersampling_precision_std, undersampling_PAI_mean, undersampling_PAI_std])
  
    np.savetxt(RF_undersampling_coverage_path, undersampling_cov_list,  delimiter=",")
#%%
#coverage:  0.05
#Hitrate:  0.233024841734 0.214323270828
#Precision:  0.00235500802966 0.00210084578462
#PAI:  4.66463337033 4.29026997166

#coverage:  0.1
#Hitrate:  0.384000440968 0.246245711941
#Precision:  0.00197851893725 0.00146111608562
#PAI:  3.83962608412 2.46221451279

#0.20000000000000001, 
#0.59056340214127101, 0.26699546021114118, 
#0.0015249401060593826, 0.00096607584914059892, 
#2.9525260927742654, 1.3348457761984589

    #%%
    coverages = [v[0] for v in undersampling_cov_list] 
    hitrates = [v[1] for v in undersampling_cov_list] 
    #print(coverages)
    auc_RF_undersampling = auc(coverages, hitrates)
    print(auc_RF_undersampling)
    #0.772589556865
#%%
    heuristic_undersampling_cov_list = []
    for pred_cov in tqdm(pred_cov_list):

        undersampling_coverage, undersampling_hitrate_mean, undersampling_hitrate_std, undersampling_precision_mean, undersampling_precision_std, undersampling_PAI_mean, undersampling_PAI_std = PAI(np.array(RF_heuristic_undersampling_pred_proba),y_test,pred_cov, days, cells)
        
        print('\n')
        print('coverage: ', undersampling_coverage)
        
        print('Hitrate: ', undersampling_hitrate_mean, undersampling_hitrate_std)
        print('Precision: ', undersampling_precision_mean, undersampling_precision_std)
        print('PAI: ', undersampling_PAI_mean, undersampling_PAI_std)
        
        heuristic_undersampling_cov_list.append([undersampling_coverage, undersampling_hitrate_mean, undersampling_hitrate_std, undersampling_precision_mean, undersampling_precision_std, undersampling_PAI_mean, undersampling_PAI_std])
  
    np.savetxt(RF_heuristic_undersampling_coverage_path, heuristic_undersampling_cov_list,  delimiter=",")

#coverage:  0.05
#Hitrate:  0.124627262537 0.170430580486
#Precision:  0.0012772011511 0.00166091578704
#PAI:  2.49475756901 3.41163700464

#coverage:  0.1
#Hitrate:  0.218138479614 0.218870231336
#Precision:  0.00112250666236 0.00114679977166
#PAI:  2.18116988138 2.18848667766

#coverage:  0.2
#Hitrate:  0.431888959553 0.264718034487
#Precision:  0.00113596597486 0.00085699787724
#PAI:  2.15923204458 1.32345976946

    #%%
    coverages = [v[0] for v in heuristic_undersampling_cov_list] 
    hitrates = [v[1] for v in heuristic_undersampling_cov_list] 
    #print(coverages)
    auc_RF_heuristic_undersampling = auc(coverages, hitrates)
    print(auc_RF_heuristic_undersampling)
    #0.721356431403
#%%
    oversampling_cov_list = []
    for pred_cov in tqdm(pred_cov_list):

        oversampling_coverage, oversampling_hitrate_mean, oversampling_hitrate_std, oversampling_precision_mean, oversampling_precision_std, oversampling_PAI_mean, oversampling_PAI_std = PAI(np.array(RF_oversampling_pred_proba),y_test,pred_cov, days, cells)
        
        print('\n')
        print('coverage: ', oversampling_coverage)
        
        print('Hitrate: ', oversampling_hitrate_mean, oversampling_hitrate_std)
        print('Precision: ', oversampling_precision_mean, oversampling_precision_std)
        print('PAI: ', oversampling_PAI_mean, oversampling_PAI_std)
        
        oversampling_cov_list.append([oversampling_coverage, oversampling_hitrate_mean, oversampling_hitrate_std, oversampling_precision_mean, oversampling_precision_std, oversampling_PAI_mean, oversampling_PAI_std])
  
    np.savetxt(RF_oversampling_coverage_path, oversampling_cov_list,  delimiter=",")
#coverage:  0.05
#Hitrate:  0.105145063649 0.147162732326
#Precision:  0.00108858494735 0.00136196583343
#PAI:  2.1047677534 2.94586700271

#coverage:  0.1
#Hitrate:  0.164002698122 0.184402608488
#Precision:  0.000885622762389 0.000967922039089
#PAI:  1.6398654022 1.84384440743   

#coverage:  0.2
#Hitrate:  0.276692054151 0.222321036732
#Precision:  0.000726802874909 0.000624001365433
#PAI:  1.38332396925 1.11149566591 

#%%
coverage:  0.05
Hitrate:  0.154604461264 0.181282102476
Precision:  0.00152509673317 0.00162417486158
PAI:  3.09483368318 3.62886007501

coverage:  0.1
Hitrate:  0.252302513061 0.226736745576
Precision:  0.00125440792484 0.00112199980643
PAI:  2.5227765567 2.2671440698

coverage:  0.2
Hitrate:  0.408092639752 0.256364631681
Precision:  0.00102425368112 0.000732247213789
PAI:  2.04026216791 1.28169687041

#%%
    coverages = [v[0] for v in oversampling_cov_list] 
    hitrates = [v[1] for v in oversampling_cov_list] 
    #print(coverages)
    auc_RF_oversampling = auc(coverages, hitrates)
    print(auc_RF_oversampling)
    #0.592926356336
#######################################################
    #%%
    heuristic_oversampling_cov_list = []
    for pred_cov in tqdm(pred_cov_list):

        heuristic_oversampling_coverage, heuristic_oversampling_hitrate_mean, heuristic_oversampling_hitrate_std, heuristic_oversampling_precision_mean, heuristic_oversampling_precision_std, heuristic_oversampling_PAI_mean, heuristic_oversampling_PAI_std = PAI(np.array(RF_heuristic_oversampling_pred_proba),y_test,pred_cov, days, cells)
        
        print('\n')
        print('coverage: ', heuristic_oversampling_coverage)
        
        print('Hitrate: ', heuristic_oversampling_hitrate_mean, heuristic_oversampling_hitrate_std)
        print('Precision: ', heuristic_oversampling_precision_mean, heuristic_oversampling_precision_std)
        print('PAI: ', heuristic_oversampling_PAI_mean, heuristic_oversampling_PAI_std)
        
        heuristic_oversampling_cov_list.append([heuristic_oversampling_coverage, heuristic_oversampling_hitrate_mean, heuristic_oversampling_hitrate_std, heuristic_oversampling_precision_mean, heuristic_oversampling_precision_std, heuristic_oversampling_PAI_mean, heuristic_oversampling_PAI_std])
  
    np.savetxt(RF_heuristic_oversampling_coverage_path, heuristic_oversampling_cov_list,  delimiter=",")
#coverage:  0.05
#Hitrate:  0.123654502753 0.172808709042
#Precision:  0.00120714370399 0.00150434197675
#PAI:  2.4752851054 3.45924179106

#coverage:  0.1
#Hitrate:  0.182559596596 0.195437950889
#Precision:  0.000939460012383 0.00097739818523
#PAI:  1.82541610429 1.95418695919

#coverage:  0.2
#Hitrate:  0.289501489706 0.236059270007
#Precision:  0.000741608118657 0.000642950949504
#PAI:  1.44736483696 1.18018006468

#%%
coverage:  0.05
Hitrate:  0.162439131906 0.181707238878
Precision:  0.00159515418027 0.00172704890001
PAI:  3.25166617301 3.63737034985

coverage:  0.1
Hitrate:  0.264895134465 0.222567396487
Precision:  0.00131632076234 0.00115115132238
PAI:  2.64869036422 2.22545468665

coverage:  0.2
Hitrate:  0.412627627382 0.250865679845
Precision:  0.00105790196237 0.000778474348296
PAI:  2.06293487207 1.25420482007

#%%
    coverages = [v[0] for v in heuristic_oversampling_cov_list] 
    hitrates = [v[1] for v in heuristic_oversampling_cov_list] 
    #print(coverages)
    auc_RF_heuristic_oversampling = auc(coverages, hitrates)
    print(auc_RF_heuristic_oversampling)
    #0.590372757176
#%%
    costsensitive_cov_list = []
    for pred_cov in tqdm(pred_cov_list):

        costsensitive_coverage, costsensitive_hitrate_mean, costsensitive_hitrate_std, costsensitive_precision_mean, costsensitive_precision_std, costsensitive_PAI_mean, costsensitive_PAI_std = PAI(np.array(RF_costsensitive_pred_proba),y_test,pred_cov, days, cells)
        
        print('\n')
        print('coverage: ', costsensitive_coverage)
        
        print('Hitrate: ', costsensitive_hitrate_mean, costsensitive_hitrate_std)
        print('Precision: ', costsensitive_precision_mean, costsensitive_precision_std)
        print('PAI: ', costsensitive_PAI_mean, costsensitive_PAI_std)
        
        costsensitive_cov_list.append([costsensitive_coverage, costsensitive_hitrate_mean, costsensitive_hitrate_std, costsensitive_precision_mean, costsensitive_precision_std, costsensitive_PAI_mean, costsensitive_PAI_std])
  
    np.savetxt(RF_costsensitive_coverage_path, costsensitive_cov_list,  delimiter=",")
#coverage:  0.05
#Hitrate:  0.212721810673 0.213598929099
#Precision:  0.00226878347938 0.00224634838253
#PAI:  4.25821234027 4.27577027894

#coverage:  0.1
#Hitrate:  0.35748359109 0.242162525723
#Precision:  0.00187892002477 0.0014555386178
#PAI:  3.57448371032 2.42138667346

#coverage:  0.2
#Hitrate:  0.576458544491 0.270064823041
#Precision:  0.00149532961856 0.000969803941792
#PAI:  2.88200875273 1.35019107835

#%%
coverage:  0.05
Hitrate:  0.209133853396 0.213192328473
Precision:  0.00220411506666 0.00222068107951
PAI:  4.18638950319 4.26763104867

coverage:  0.1
Hitrate:  0.361184417434 0.244407459314
Precision:  0.00189237933726 0.00145723274674
PAI:  3.61148832763 2.44383379761

coverage:  0.2
Hitrate:  0.587005693153 0.262207896113
Precision:  0.00150744299981 0.000944218496908
PAI:  2.9347393004 1.31091031412
    #%%
    #print(costsensitive_cov_list[0][0])
    #print(costsensitive_cov_list[1][0])
    #print(costsensitive_cov_list[19][0])
    coverages = [v[0] for v in costsensitive_cov_list] 
    hitrates = [v[1] for v in costsensitive_cov_list] 
    #print(coverages)
    auc_RF_costsensitive = auc(coverages, hitrates)
    print(auc_RF_costsensitive)
    #0.768696091591
      #%%
    
    AB_undersampling_cov_list = []
    for pred_cov in tqdm(pred_cov_list):

        undersampling_coverage, undersampling_hitrate_mean, undersampling_hitrate_std, undersampling_precision_mean, undersampling_precision_std, undersampling_PAI_mean, undersampling_PAI_std = PAI(np.array(AB_undersampling_pred_proba),y_test,pred_cov, days, cells)
        
        print('\n')
        print('coverage: ', undersampling_coverage)
        
        print('Hitrate: ', undersampling_hitrate_mean, undersampling_hitrate_std)
        print('Precision: ', undersampling_precision_mean, undersampling_precision_std)
        print('PAI: ', undersampling_PAI_mean, undersampling_PAI_std)
        
        AB_undersampling_cov_list.append([undersampling_coverage, undersampling_hitrate_mean, undersampling_hitrate_std, undersampling_precision_mean, undersampling_precision_std, undersampling_PAI_mean, undersampling_PAI_std])
  
    np.savetxt(AB_undersampling_coverage_path, AB_undersampling_cov_list,  delimiter=",")
#%%
print(AB_undersampling_cov_list)
#coverage:  0.05
#Hitrate:  0.232482249081 0.222359396581
#Precision:  0.00233345189209 0.00221297795459
#PAI:  4.65377188544 4.45113513985

#coverage:  0.1
#Hitrate:  0.377104805486 0.252712068149
#Precision:  0.00193814099976 0.0015046042575
#PAI:  3.77067652303 2.52687170408

#0.20000000000000001, 
#0.58667153656907756, 0.27513068714614236, 
#0.0015303238310587096, 0.0010140223169309954, 
#2.9330686820884564, 1.3755179033725118

#%%
    coverages = [v[0] for v in AB_undersampling_cov_list] 
    hitrates = [v[1] for v in AB_undersampling_cov_list] 
    #print(coverages)
    auc_AB_undersampling = auc(coverages, hitrates)
    print(auc_AB_undersampling)
#%%    
    LR_undersampling_cov_list = []
    for pred_cov in tqdm(pred_cov_list):

        undersampling_coverage, undersampling_hitrate_mean, undersampling_hitrate_std, undersampling_precision_mean, undersampling_precision_std, undersampling_PAI_mean, undersampling_PAI_std = PAI2(np.array(LR_undersampling_pred_proba),y_test,pred_cov, days, cells)
        
        print('\n')
        print('coverage: ', undersampling_coverage)
        
        print('Hitrate: ', undersampling_hitrate_mean, undersampling_hitrate_std)
        print('Precision: ', undersampling_precision_mean, undersampling_precision_std)
        print('PAI: ', undersampling_PAI_mean, undersampling_PAI_std)
        
        LR_undersampling_cov_list.append([undersampling_coverage, undersampling_hitrate_mean, undersampling_hitrate_std, undersampling_precision_mean, undersampling_precision_std, undersampling_PAI_mean, undersampling_PAI_std])
  
    np.savetxt(LR_undersampling_coverage_path, LR_undersampling_cov_list,  delimiter=",")
#%%
print(LR_undersampling_cov_list)
#coverage:  0.05
#Hitrate:  0.245901324692 0.236397536464
#Precision:  0.00253284616462 0.00246671828053
#PAI:  4.92239160612 4.73214713526

#coverage:  0.1
#Hitrate:  0.371218944989 0.252841148837
#Precision:  0.00191122237476 0.00145902920656
#PAI:  3.71182371694 2.52816238379  

#0.20000000000000001, 
#0.57333347056707717, 0.27398265577010344, 
#0.0015034052060620743, 0.001007847303945171, 
#2.8663849225543183, 1.3697783120250147  
#%% changed to PAI2 to test   
    LRl1_undersampling_cov_list = []
    for pred_cov in tqdm(pred_cov_list):

        undersampling_coverage, undersampling_hitrate_mean, undersampling_hitrate_std, undersampling_precision_mean, undersampling_precision_std, undersampling_PAI_mean, undersampling_PAI_std = PAI2(np.array(LRl1_undersampling_pred_proba),y_test,pred_cov, days, cells)
        
        print('\n')
        print('coverage: ', undersampling_coverage)
        
        print('Hitrate: ', undersampling_hitrate_mean, undersampling_hitrate_std)
        print('Precision: ', undersampling_precision_mean, undersampling_precision_std)
        print('PAI: ', undersampling_PAI_mean, undersampling_PAI_std)
        
        LRl1_undersampling_cov_list.append([undersampling_coverage, undersampling_hitrate_mean, undersampling_hitrate_std, undersampling_precision_mean, undersampling_precision_std, undersampling_PAI_mean, undersampling_PAI_std])
  
    np.savetxt(LRl1_undersampling_coverage_path, LRl1_undersampling_cov_list,  delimiter=",")
#%%
print(LRl1_undersampling_cov_list)
#coverage:  0.05
#Hitrate:  0.217960060583 0.216713232933
#Precision:  0.00223106023863 0.00219118429997
#PAI:  4.36307032516 4.33811163913

#coverage:  0.1
#Hitrate:  0.340748970974 0.252829501298
#Precision:  0.00175778621228 0.00142711623459
#PAI:  3.40715399647 2.52804591987

#.20000000000000001, 
#0.54351449628088977, 0.25696047105629355, 
#0.0014064981560741874, 0.00092242649728152299, 
#2.7173047402732755, 1.2846757737686321
    #%%
    print(sum(y_test_maj_class))
    print(sum(y_test))
   
    #%%needed to change to PAI2
    dummy_cov_list = []
    for pred_cov in tqdm(pred_cov_list):
        dummy_coverage, dummy_hitrate_mean, dummy_hitrate_std, dummy_precision_mean, dummy_precision_std, dummy_PAI_mean, dummy_PAI_std = PAI2(np.array(y_test_maj_class),y_test,pred_cov, days, cells)
    
        print('\n')
        print('coverage: ', dummy_coverage)

        print('\n')
        print('DUMMY Hitrate: ', dummy_hitrate_mean, dummy_hitrate_std)
        print('DUMMY Precision: ', dummy_precision_mean, dummy_precision_std)
        print('DUMMY PAI: ', dummy_PAI_mean, dummy_PAI_std)
              
        dummy_cov_list.append([dummy_coverage, dummy_hitrate_mean, dummy_hitrate_std, dummy_precision_mean, dummy_precision_std, dummy_PAI_mean, dummy_PAI_std])
     
    np.savetxt(dummy_coverage_path, dummy_cov_list, delimiter=",")
#%%
print(dummy_cov_list)    
#coverage:  0.05
#DUMMY Hitrate:  0.0627389535176 0.120584424035
#DUMMY Precision:  0.000689796402281 0.00128453600763
#DUMMY PAI:  1.25589277959 2.41382903261

#coverage:  0.1
#DUMMY Hitrate:  0.139315936037 0.178113134304
#DUMMY Precision:  0.000742954049907 0.000954970233326
#DUMMY PAI:  1.39302210329 1.78095586212

#0.20000000000000001, 
#0.24623180780967663, 0.21760747205906175, 
#0.0006487388624189076, 0.00061649528391167368, 
#1.2310377425913341, 1.0879301644962649

    #%% changed to PAI2 to test
    RF_ensemble_cov_list_only_crime = []
    for pred_cov in tqdm(pred_cov_list):

        print('\n')
        print('coverage: ', pred_cov)
        coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std = PAI2(np.array(final_pred_proba),y_test,pred_cov, days, cells)
        
        
        print('Hitrate: ', hitrate_mean, hitrate_std)
        print('Precision: ', precision_mean, precision_std)
        print('PAI: ', PAI_mean, PAI_std)
        
        RF_ensemble_cov_list.append([coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std])
 
    
    np.savetxt(RF_ensemble_coverage_path, RF_ensemble_cov_list, delimiter=",")

#%%
    LRl1_ensemble = pd.read_csv(LRl1_ensemble_coverage_path,header=None)
    LRl1_ensemble.columns = ['coverage','hitrate_mean','hitrate_std','precision_mean', 'precision_std', 'PAI_mean', 'PAI_std']

    LR_ensemble = pd.read_csv(LR_ensemble_coverage_path,header=None)
    LR_ensemble.columns = ['coverage','hitrate_mean','hitrate_std','precision_mean', 'precision_std', 'PAI_mean', 'PAI_std']

    AB_ensemble = pd.read_csv(AB_ensemble_coverage_path,header=None)
    AB_ensemble.columns = ['coverage','hitrate_mean','hitrate_std','precision_mean', 'precision_std', 'PAI_mean', 'PAI_std']
#%%
    RF_ensemble_only_crime = pd.read_csv(RF_ensemble_coverage_path_only_crime,header=None)
    RF_ensemble_only_crime.columns = ['coverage','hitrate_mean','hitrate_std','precision_mean', 'precision_std', 'PAI_mean', 'PAI_std']

    RF_ensemble_only_temporal = pd.read_csv(RF_ensemble_coverage_path_only_temporal,header=None)
    RF_ensemble_only_temporal.columns = ['coverage','hitrate_mean','hitrate_std','precision_mean', 'precision_std', 'PAI_mean', 'PAI_std']

    RF_ensemble_only_spatial = pd.read_csv(RF_ensemble_coverage_path_only_spatial,header=None)
    RF_ensemble_only_spatial.columns = ['coverage','hitrate_mean','hitrate_std','precision_mean', 'precision_std', 'PAI_mean', 'PAI_std']

    #%%
    RF_ensemble = pd.read_csv(RF_ensemble_coverage_path,header=None)
    RF_ensemble.columns = ['coverage','hitrate_mean','hitrate_std','precision_mean', 'precision_std', 'PAI_mean', 'PAI_std']

    RF_undersampling = pd.read_csv(RF_undersampling_coverage_path,header=None)
    RF_undersampling.columns = ['coverage','hitrate_mean','hitrate_std','precision_mean', 'precision_std', 'PAI_mean', 'PAI_std']

    dummy = pd.read_csv(dummy_coverage_path,header=None)
    dummy.columns = ['coverage','hitrate_mean','hitrate_std','precision_mean', 'precision_std', 'PAI_mean', 'PAI_std']

    #%%    
    #print coverage area - hit rate curve
    
    fig, axs = plt.subplots(1, 1)
    plt.plot(RF_ensemble.coverage, RF_ensemble.hitrate_mean, label='RF ensemble', linewidth=2, color = 'black', ls='-')
    plt.plot(AB_ensemble.coverage, AB_ensemble.hitrate_mean, label='AB ensemble', linewidth=2, color = 'black', ls='--')
    
    plt.plot(LR_ensemble.coverage, LR_ensemble.hitrate_mean, label='RF ensemble', linewidth=2, color = 'black', ls='-.')
    plt.plot(LRl1_ensemble.coverage, LRl1_ensemble.hitrate_mean, label='RF ensemble', linewidth=2, color = 'black', ls='-.')
    
    plt.plot(dummy.coverage, dummy.hitrate_mean, label='Majority class', linewidth=2, color = 'grey', ls=':')
   
    plt.legend(loc='middle right')
    plt.xlim([0,1])
    plt.ylim([-0,1])
    plt.grid(color='grey', linestyle='-', linewidth=2, alpha=0.1)
    plt.ylabel('Hit Rate')
    plt.xlabel('Coverage Area')
    #axs.set_aspect('equal', 'box')
    plt.tight_layout()
    plt.yticks(np.arange(0, 1.1, step=0.1))
    plt.xticks(np.arange(0, 1.1, step=0.1))
    plt.show()

    print('Done!')
    #%%
    urb0 = pd.read_csv(path0,header=None)
    urb0.columns = ['coverage','hitrate_mean','hitrate_std','precision_mean', 'precision_std', 'PAI_mean', 'PAI_std']
    urb1 = pd.read_csv(path1,header=None)
    urb1.columns = ['coverage','hitrate_mean','hitrate_std','precision_mean', 'precision_std', 'PAI_mean', 'PAI_std']
    urb2 = pd.read_csv(path2,header=None)
    urb2.columns = ['coverage','hitrate_mean','hitrate_std','precision_mean', 'precision_std', 'PAI_mean', 'PAI_std']

    #%%    
    #print coverage area - hit rate curve
    
    fig, axs = plt.subplots(1, 1)
    #plt.plot(RF_ensemble.coverage, RF_ensemble.hitrate_mean, label='RF ensemble', linewidth=2, color = 'black', ls='-')
    #plt.fill_between(RF_ensemble.coverage, RF_ensemble.hitrate_mean - RF_ensemble.hitrate_std, RF_ensemble.hitrate_mean + RF_ensemble.hitrate_std, color = 'blue', alpha = 0.4)
    #plt.plot(RF_undersampling.coverage, RF_undersampling.hitrate_mean, label='RF undersampling', linewidth=2, color = 'black', ls='-.')
    #plt.plot(dummy.coverage, dummy.hitrate_mean, label='Majority class', linewidth=2, color = 'red', ls=':')
   
    plt.plot(RF_ensemble.coverage, RF_ensemble.hitrate_mean, label='All', linewidth=2, color = 'black', ls='-')
   
    plt.plot(urb0.coverage, urb0.hitrate_mean, label='Low Urbanization', linewidth=2, color = 'black', ls='--')
    plt.plot(urb1.coverage, urb1.hitrate_mean, label='Medium Urbanization', linewidth=2, color = 'black', ls='-.')
    plt.plot(urb2.coverage, urb2.hitrate_mean, label='High Urbanization', linewidth=2, color = 'black', ls=':')
   
    plt.legend(loc='middle right')
    plt.xlim([0,1])
    plt.ylim([-0,1])
    plt.grid(color='grey', linestyle='-', linewidth=2, alpha=0.1)
    plt.ylabel('Hit Rate')
    plt.xlabel('Coverage Area')
    #axs.set_aspect('equal', 'box')
    plt.tight_layout()
    plt.yticks(np.arange(0, 1.1, step=0.1))
    plt.xticks(np.arange(0, 1.1, step=0.1))
    plt.show()

    print('Done!')
    
        #%%    
    #print coverage area - hit rate curve
    
    fig, axs = plt.subplots(1, 1)
    #plt.plot(RF_ensemble.coverage, RF_ensemble.hitrate_mean, label='Ensemble of under-sampling', linewidth=2, color = 'black', ls='-')
    #plt.fill_between(RF_ensemble.coverage, RF_ensemble.hitrate_mean - RF_ensemble.hitrate_std, RF_ensemble.hitrate_mean + RF_ensemble.hitrate_std, color = 'blue', alpha = 0.1)
    #plt.plot(RF_undersampling.coverage, RF_undersampling.hitrate_mean, label='Undersampling', linewidth=2, color = 'black', ls='-.')
    ###plt.plot(dummy.coverage, dummy.hitrate_mean, label='Majority class', linewidth=2, color = 'red', ls=':')
   
    plt.plot(RF_ensemble.coverage, RF_ensemble.hitrate_mean, label='All features', linewidth=2, color = 'black', ls='-')
    plt.plot(RF_ensemble_only_spatial.coverage, RF_ensemble_only_spatial.hitrate_mean, label='Spatial features', linewidth=2, color = 'black', ls='--')
    plt.plot(RF_ensemble_only_crime.coverage, RF_ensemble_only_crime.hitrate_mean, label='Crime features', linewidth=2, color = 'black', ls='-.')
    plt.plot(RF_ensemble_only_temporal.coverage, RF_ensemble_only_temporal.hitrate_mean, label='Temporal features', linewidth=2, color = 'black', ls=':')
    
    plt.legend(loc='middle right')
    plt.xlim([0,1])
    plt.ylim([-0,1])
    plt.grid(color='grey', linestyle='-', linewidth=2, alpha=0.1)
    plt.ylabel('Hit Rate')
    plt.xlabel('Coverage Area')
    #axs.set_aspect('equal', 'box')
    plt.tight_layout()
    plt.yticks(np.arange(0, 1.1, step=0.1))
    plt.xticks(np.arange(0, 1.1, step=0.1))
    plt.show()

    print('Done!')
