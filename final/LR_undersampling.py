#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 19 13:50:29 2018

@author: ckadar
"""

import pandas as pd
import numpy as np

from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression
from sklearn.grid_search import GridSearchCV

from imblearn.ensemble import EasyEnsemble

data_path = 'dataset_final.csv'
LR_undersampling_proba_path = "proba_LR_undersampling_all_features.csv"
LR_undersampling_proba_training_path = "proba_training_LR_undersampling_all_features.csv"

def undersamp_easy_ensemble(n_subsets, X_train, y_train):
    print('Resampling...')
   # iteratively select a random subset, replacement = False by default
    ee = EasyEnsemble(n_subsets=n_subsets, random_state=42)
    X_res, y_res = ee.fit_sample(X_train, y_train)
    for i in range(n_subsets):
        print('Balanced Dataset: ', i)
        print('Class 0: ', np.count_nonzero(y_res[i] == 0))
        print('Class 1: ', np.count_nonzero(y_res[i] == 1))
    return X_res,y_res

#%%    
def load_data():
    ##%%
    print('---------------------------------')
    print("Reading in data...")
    data = pd.read_csv(data_path,index_col=[1,2]).drop(['Unnamed: 0'],axis=1)  
    print(data.info)
    ##%%

    print(data.shape)
    ##(11123304, 72)
    
    all_columns = data.columns
    print(all_columns)
    
    features = data.drop(['offence','probability','offence_ids','pred_class','probability_norm','pred_class_norm','pred_class_bin','to_date'],axis=1)
    
    target = data['to_date']
    target = target.astype(int)

    print('Features shape: ',features.shape,'Target shape: ', target.shape)
    ##Features shape:  (11123304, 64) Target shape:  (11123304,)
    ##%%
    
    feat_names = features.columns
    
    print('Total Number of cells: ', features.loc['2014-01-14',:].shape[0])
    print('Total Number of days: ', features.loc[(slice('2014-01-14','2017-01-13'),1429),:].shape[0])
    print('Number of training days: ', features.loc[(slice('2014-01-14','2016-01-13'),1429),:].shape[0])
    print('Number of testing days: ', features.loc[(slice('2016-01-14','2017-01-13'),1429),:].shape[0])
    ##Total Number of cells:  10149
    ##Total Number of days:  1096
    ##Number of training days:  730
    ##Number of testing days:  366
    
    print(feat_names)
    print(len(feat_names))
    
    ##%%
    print ('Class distributions - Original dataset of built land-use')
    print('Class 0: ', target.value_counts()[0], '(',float(target.value_counts()[0])/target.shape[0]*100, '% )')
    print('Class 1: ', target.value_counts()[1], '(',float(target.value_counts()[1])/target.shape[0]*100, '% )')
    ##Class 0:  11117038 ( 99.94366781668468 % )
    ##Class 1:  6266 ( 0.0563321833153171 % )
    
    ## split dataset into training and testing data
    X_train = features.loc['2014-01-14':'2016-01-13',:].values
    y_train = target.loc['2014-01-14':'2016-01-13',:].values
    X_test = features.loc['2016-01-14':'2017-01-13',:].values
    y_test = target.loc['2016-01-14':'2017-01-13',:].values

    tot = len(y_train)
    print('Class distributions - Training')
    print('Class 0: ', np.count_nonzero(y_train == 0), '(',float(np.count_nonzero(y_train == 0))/tot*100, '% )')
    print('Class 1: ', np.count_nonzero(y_train == 1), '(',float(np.count_nonzero(y_train == 1))/tot*100, '% )')
    ##Class 0:  7404388 ( 99.94085387992878 % )
    ##Class 1:  4382 ( 0.05914612007121291 % )
    
    tot = len(y_test)
    print('Class distributions - Testing ')
    print('Class 0: ', np.count_nonzero(y_test == 0), '(',float(np.count_nonzero(y_test == 0))/tot*100, '% )')
    print('Class 1: ', np.count_nonzero(y_test == 1), '(',float(np.count_nonzero(y_test == 1))/tot*100, '% )')
    ##Class 0:  3712650 ( 99.94928031349289 % )
    ##Class 1:  1884 ( 0.05071968650710
    
    return X_train,y_train,X_test,y_test,feat_names

#%%
#def main():

    #read in data
    X_train, y_train, X_test, y_test, all_features = load_data()
    print(sum(y_test))
#%%
    scaling = StandardScaler()
    #for non-DT based models we need to do feature scalling
    X_train = scaling.fit_transform(X_train) ##here use fit and transform
    X_test = scaling.transform(X_test)
#%%    
    # create a number of balanced subsets of the training data by undersampling
    n_subsets = 1
    X_res, y_res = undersamp_easy_ensemble(n_subsets, X_train, y_train)
#%%  
    
    print(y_res.shape)
    # define grid containing a set of hyperparameters
    param_grid = { 
                  'penalty': ['l2'],
                  'C': [1000, 100, 10, 1, 0.1, 0.01, 0.001]
                }

    print('Training the LR model...')

    # define the estimator
    clf = LogisticRegression(n_jobs=-1)

    # define the CV estimator
    CV_clf = GridSearchCV(estimator=clf, param_grid=param_grid, cv= 5, scoring='roc_auc')
#%%
    # fit the CV estimator
    CV_clf.fit(X_res[0],y_res[0])
    
    #extract best score, best parameters, best model of the CV on this subset
    LR_best_score = CV_clf.best_score_
    print('        The best score is: ', LR_best_score)
    
    LR_best_params = CV_clf.best_params_
    print('        The best parameters are: ', LR_best_params)
    
    LR_best_model = CV_clf.best_estimator_
    print('        The best model is: ', LR_best_model)
    
    #and test it on the test set
    #y_preds = best_model.predict(X_test)
    LR_y_preds_proba = LR_best_model.predict_proba(X_test)

    #test_score = roc_auc_score(y_test,y_preds)
    #append the score of the best model on the test set
    #test_scores.append(test_score) 
    #print(test_score)
    
    #test order of classes
    #print(CV_clf.classes_)
    
    print("Saving data...")
    # save  probabilities 
    np.savetxt(LR_undersampling_proba_path, LR_y_preds_proba, delimiter=",")

#%%    
    #save also predictions on training set -- actually on the subsample
    LR_y_training_proba = LR_best_model.predict_proba(X_res[0]) 
    np.savetxt(LR_undersampling_proba_training_path, LR_y_training_proba, delimiter=",")

    
#main()

	