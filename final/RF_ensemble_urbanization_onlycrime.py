#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 27 19:57:38 2018

@author: ckadar
"""

import pandas as pd
import numpy as np

from sklearn.preprocessing import StandardScaler
from sklearn.ensemble import RandomForestClassifier
from sklearn.grid_search import GridSearchCV

from imblearn.ensemble import EasyEnsemble

data_path = 'dataset_final.csv'
RF_ensemble_proba_path ="avg_test_proba_RF_ensemble_onlycrime_features.csv"
RF_ensemble_features_path = "feat_impo_RF_ensemble_onlycrime_features.npy"

def undersamp_easy_ensemble(n_subsets, X_train, y_train):
    print('Resampling...')
    # iteratively select a random subset, replacement = False by default
    ee = EasyEnsemble(n_subsets=n_subsets, random_state=42)
    X_res, y_res = ee.fit_sample(X_train, y_train)
    for i in range(n_subsets):
        print('Balanced Dataset: ', i)
        print('Class 0: ', np.count_nonzero(y_res[i] == 0))
        print('Class 1: ', np.count_nonzero(y_res[i] == 1))
    return X_res,y_res


def feature_imp(avg_impo, avg_impo_std, feat_names):
    print('Feature ranking...')
    indices = np.argsort(avg_impo)[::-1]
    impo_list = []
    
    for f in range(len(feat_names)):
        print("%d. Feature: %s (%f)" % (f + 1, feat_names[indices[f]], avg_impo[indices[f]])), 'STD: ', avg_impo_std[indices[f]]
        impo_list.append([feat_names[indices[f]], avg_impo[indices[f]], avg_impo_std[indices[f]]]) 
        
    # plot the feature importances of the forest
    
    #plt.figure(figsize=(30,8))
    #plt.title("Feature importances")
    #plt.bar(range(len(feat_names)), avg_impo[indices],color="r", yerr=avg_impo_std[indices], align="center")
    #plt.xticks(range(len(feat_names)), feat_names[indices])
    #plt.xlim([-1,20])
    
    return impo_list

def only_load_data():
    ##%%
    print('---------------------------------')
    print("Reading in data...")
    data = pd.read_csv(data_path,index_col=[1,2]).drop(['Unnamed: 0'],axis=1)  
    print(data.info)
    ##%%

    print(data.shape)
    ##(11123304, 72)
    
    all_columns = data.columns
    print(all_columns)
    
    print('adding urb_lev column...')
    data['urb_lev'] = np.zeros(data.shape[0]).astype(int)
    # assign level of urbanization according to natural breaks thresholds
    #   jenks algorithm: [0.0, 18.25, 54.25, 250.75]
    #   percentiles: [0.0 2.25 16.75 250.75]
    print('   PERCENTILES')
    print('   33%: ', np.percentile(data.popdens, 33.33))
    print('   66%: ', np.percentile(data.popdens, 66.66))
    data.loc[(data['popdens'] >= np.percentile(data.popdens, 33.33)) & (data['popdens'] < np.percentile(data.popdens, 66.66)), 'urb_lev'] = 1
    data.loc[(data['popdens'] >= np.percentile(data.popdens, 66.66)), 'urb_lev'] = 2

    return data

def split_data(data, urb_level): 
    # only one type of urbanization level!
    features = data[data['urb_lev']==urb_level].drop(['offence','probability','offence_ids','pred_class','probability_norm','pred_class_norm','pred_class_bin','urb_lev','to_date'],axis=1)
    target = data[data['urb_lev']==urb_level].to_date
    target = target.astype(int)
    
    temporal_features = ['dow', 'holiday', 'event', 'temp', 'hum', 'discomf', 'daylight', 'moon']
    
    prior_features = ['prior1d', 'prior3d', 'prior7d', 'prior14d']

    poi_infra_features = ['buildgs_areafrac', 'buildgs_dens', 'land_swim', 'land_oldtown',
       'land_green', 'land_business', 'land_indust', 'land_central',
       'land_public', 'land_park', 'land_special', 'land_river', 'land_resi1',
       'land_resi2', 'land_resi3', 'land_mix2', 'land_mix3', 'land_no_use',
       'land_dividx', 'poi_infra', 'poi_shop', 'poi_gastro', 'poi_public',
       'poi_edu', 'intersection', 'highway_exit', 'border_cross', 'road_type',
       'pub_trans', 'pub_hous']
       
    demographic_features = [ 'popage_1', 'popage_2',
       'popage_3', 'popage_4', 'popage_dividx', 'popbirth_nonCH', 'popcit_CH',
       'popcit_EU', 'popcit_dividx', 'popcit_europ', 'popcit_noneurop',
       'popdens', 'popmale', 'popstab', 'busi_sec1', 'busi_sec2', 'busi_sec3',
       'busidens', 'busisec_dividx', 'empldens', 'emplmale', 'emplsec_dividx']

    features = features[prior_features]

    print('Features shape: ',features.shape,'Target shape: ', target.shape)
    ##Features shape:  (11123304, 64) Target shape:  (11123304,)
    ##%%
    
    feat_names = features.columns
    
    print('Total Number of cells: ', features.loc['2014-01-14',:].shape[0])
    print('Total Number of days: ', features.loc[(slice('2014-01-14','2017-01-13'),1429),:].shape[0])
    print('Number of training days: ', features.loc[(slice('2014-01-14','2016-01-13'),1429),:].shape[0])
    print('Number of testing days: ', features.loc[(slice('2016-01-14','2017-01-13'),1429),:].shape[0])
    ##Total Number of cells:  10149
    ##Total Number of days:  1096
    ##Number of training days:  730
    ##Number of testing days:  366
    
    print(feat_names)
    print(len(feat_names))
    
    ##%%
    print ('Class distributions - Original dataset of built land-use')
    print('Class 0: ', target.value_counts()[0], '(',float(target.value_counts()[0])/target.shape[0]*100, '% )')
    print('Class 1: ', target.value_counts()[1], '(',float(target.value_counts()[1])/target.shape[0]*100, '% )')
    ##Class 0:  11117038 ( 99.94366781668468 % )
    ##Class 1:  6266 ( 0.0563321833153171 % )
    
    ## split dataset into training and testing data
    X_train = features.loc['2014-01-14':'2016-01-13',:].values
    y_train = target.loc['2014-01-14':'2016-01-13',:].values
    X_test = features.loc['2016-01-14':'2017-01-13',:].values
    y_test = target.loc['2016-01-14':'2017-01-13',:].values

    tot = len(y_train)
    print('Class distributions - Training')
    print('Class 0: ', np.count_nonzero(y_train == 0), '(',float(np.count_nonzero(y_train == 0))/tot*100, '% )')
    print('Class 1: ', np.count_nonzero(y_train == 1), '(',float(np.count_nonzero(y_train == 1))/tot*100, '% )')
    ##Class 0:  7404388 ( 99.94085387992878 % )
    ##Class 1:  4382 ( 0.05914612007121291 % )
    
    tot = len(y_test)
    print('Class distributions - Testing ')
    print('Class 0: ', np.count_nonzero(y_test == 0), '(',float(np.count_nonzero(y_test == 0))/tot*100, '% )')
    print('Class 1: ', np.count_nonzero(y_test == 1), '(',float(np.count_nonzero(y_test == 1))/tot*100, '% )')
    ##Class 0:  3712650 ( 99.94928031349289 % )
    ##Class 1:  1884 ( 0.05071968650710
    
    return X_train,y_train,X_test,y_test,feat_names
    
def main():
    
    #read in data
    data = only_load_data()
    
    
    #The resulting upper threshold for the lowest level of urbanization is 2.25 residents per hectare (3’666’053 samples),
    #the medium level of urbanization is composed of cells including between 2.25 and 16.75 residents per hectare (3’745’075 samples) 
    #and finally the highest level of urbanization covers all cells with over 16.75 residents per hectare (3’712’176 samples).
    #new = data.loc[data['urb_lev']==0]
    #print(new.shape)#(3666053, 73)
    #new = data.loc[data['urb_lev']==1]
    #print(new.shape)#(3745075, 73)
    
    
    # three different models according to the level of urbanization
    for urb_level in range(3):
        
        print("processing urbanization level: " + str(urb_level))
        X_train, y_train, X_test, y_test, all_features = split_data(data, urb_level)
        print(sum(y_test))
        
        scaling = StandardScaler()
        #for non-DT based models we need to do feature scalling
        X_train = scaling.fit_transform(X_train) ##here use fit and transform
        X_test = scaling.transform(X_test)
      
        # create a number of balanced subsets of the training data by undersampling
        n_subsets = 10 #1
        X_res, y_res = undersamp_easy_ensemble(n_subsets, X_train, y_train)
    
        # define grid containing a set of hyperparameters
        feature_count = X_res.shape[2]
        print(feature_count)
    
        param_grid = {##'n_estimators': [10]
                      'n_estimators': [500, 1000, 1500],
                      'max_depth': [int(feature_count/3), int(feature_count/2), None]
                      ##'max_features': ['auto', 'sqrt', 'log2']
                    }
    
        print('Training the RF ensemble...')
        
        #test_scores = []
        test_probas = []
    
        #cv_scores = []
        
    
        importances = []
        importances_std = []
    
        for i in range(0,n_subsets):
            print('...hyperparameter tuning (subset: ', i, ')')
            
            # define the estimator
            clf = RandomForestClassifier(n_jobs=-1)
    
            # define the CV estimator
            CV_clf = GridSearchCV(estimator=clf, param_grid=param_grid, cv= 5, scoring='roc_auc')
    
            # fit the CV estimator
            CV_clf.fit(X_res[i],y_res[i])
            
            #extract best score, best parameters, best model of the CV on this subset
            best_score = CV_clf.best_score_
            print('        The best score is: ', best_score)
            
            best_params = CV_clf.best_params_
            print('        The best parameters are: ',best_params)
            
            best_model = CV_clf.best_estimator_
            print('        The best model is: ', best_model)
            
            # Rudi manually computed and refitted the best model
            #clf = RandomForestClassifier(n_estimators=best_params['n_estimators'], max_features=best_params['max_features'], n_jobs=-1)
            #print('        ...making predictions...')
            #clf.fit(X_res[i],y_res[i])
            
            #and test it on the test set
            #y_preds = best_model.predict(X_test)
            y_preds_proba = best_model.predict_proba(X_test)
        
            #test_score = roc_auc_score(y_test,y_preds)
            #append the score of the best model on the test set
            #test_scores.append(test_score) 
            #print(test_score)
            
            #append the probs of the best model on the test set
            test_probas.append(y_preds_proba)
            
            #append the best score value during CV 
            #cv_scores.append(best_score)
                    
            # append feature importance values extracted during CV
            importances.append(best_model.feature_importances_)
            importances_std.append(np.std([tree.feature_importances_ for tree in best_model.estimators_],axis=0))
    
            #test order of classes
            #print(CV_clf.classes_)
        
        # compute averages of the assemble
        #avg_test_score = np.mean(test_scores)
        #print(avg_test_score)
        avg_test_proba = np.mean( np.array( test_probas ), axis=0)
    
        #avg_cv = np.mean( np.array( cv_scores ), axis=0 )
        avg_impo = np.mean( np.array( importances ), axis=0 )
        avg_impo_std = np.mean( np.array( importances_std ), axis=0 )
          
        print("Saving data...")
        # save average probabilities 
        np.savetxt(str(urb_level) + RF_ensemble_proba_path, avg_test_proba, delimiter=",")
    
        # save average feature importance + STD + name
        print(all_features)
        features_list = feature_imp(avg_impo, avg_impo_std, all_features)
        np.save(str(urb_level) + RF_ensemble_features_path,features_list)
    

main()

	