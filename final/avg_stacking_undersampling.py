#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jul 22 21:47:48 2018

@author: ckadar
"""

import pandas as pd
import numpy as np

from imblearn.ensemble import EasyEnsemble

data_path = 'dataset_final.csv'

RF_undersampling_proba_path = "proba_RF_undersampling_all_features.csv"
RF_undersampling_features_path = "feat_impo_RF_undersampling_all_features.npy"

AB_undersampling_proba_path = "proba_AB_undersampling_all_features.csv"
AB_undersampling_features_path = "feat_impo_AB_undersampling_all_features.npy"

LR_undersampling_proba_path = "proba_LR_undersampling_all_features.csv"
LR_undersampling_coverage_path = "coverage_LR_undersampling_all_features.csv"

LRl1_undersampling_proba_path = "proba_LRl1_undersampling_all_features.csv"
LRl1_undersampling_coverage_path = "coverage_LRl1_undersampling_all_features.csv"

avg_stacking_proba_path = "proba_avg_stacking_undersampling_all_features.csv"

def load_data():
    ##%%
    print('---------------------------------')
    print("Reading in data...")
    data = pd.read_csv(data_path,index_col=[1,2]).drop(['Unnamed: 0'],axis=1)  
    print(data.info)
    ##%%

    print(data.shape)
    ##(11123304, 72)
    
    all_columns = data.columns
    print(all_columns)
    
    features = data.drop(['offence','probability','offence_ids','pred_class','probability_norm','pred_class_norm','pred_class_bin','to_date'],axis=1)
    
    target = data['to_date']
    target = target.astype(int)

    print('Features shape: ',features.shape,'Target shape: ', target.shape)
    ##Features shape:  (11123304, 64) Target shape:  (11123304,)
    ##%%
    
    feat_names = features.columns
    
    print('Total Number of cells: ', features.loc['2014-01-14',:].shape[0])
    print('Total Number of days: ', features.loc[(slice('2014-01-14','2017-01-13'),1429),:].shape[0])
    print('Number of training days: ', features.loc[(slice('2014-01-14','2016-01-13'),1429),:].shape[0])
    print('Number of testing days: ', features.loc[(slice('2016-01-14','2017-01-13'),1429),:].shape[0])
    ##Total Number of cells:  10149
    ##Total Number of days:  1096
    ##Number of training days:  730
    ##Number of testing days:  366
    
    print(feat_names)
    print(len(feat_names))
    
    ##%%
    print ('Class distributions - Original dataset of built land-use')
    print('Class 0: ', target.value_counts()[0], '(',float(target.value_counts()[0])/target.shape[0]*100, '% )')
    print('Class 1: ', target.value_counts()[1], '(',float(target.value_counts()[1])/target.shape[0]*100, '% )')
    ##Class 0:  11117038 ( 99.94366781668468 % )
    ##Class 1:  6266 ( 0.0563321833153171 % )
    
    ## split dataset into training and testing data
    X_train = features.loc['2014-01-14':'2016-01-13',:].values
    y_train = target.loc['2014-01-14':'2016-01-13',:].values
    X_test = features.loc['2016-01-14':'2017-01-13',:].values
    y_test = target.loc['2016-01-14':'2017-01-13',:].values

    tot = len(y_train)
    print('Class distributions - Training')
    print('Class 0: ', np.count_nonzero(y_train == 0), '(',float(np.count_nonzero(y_train == 0))/tot*100, '% )')
    print('Class 1: ', np.count_nonzero(y_train == 1), '(',float(np.count_nonzero(y_train == 1))/tot*100, '% )')
    ##Class 0:  7404388 ( 99.94085387992878 % )
    ##Class 1:  4382 ( 0.05914612007121291 % )
    
    tot = len(y_test)
    print('Class distributions - Testing ')
    print('Class 0: ', np.count_nonzero(y_test == 0), '(',float(np.count_nonzero(y_test == 0))/tot*100, '% )')
    print('Class 1: ', np.count_nonzero(y_test == 1), '(',float(np.count_nonzero(y_test == 1))/tot*100, '% )')
    ##Class 0:  3712650 ( 99.94928031349289 % )
    ##Class 1:  1884 ( 0.05071968650710
    
    return X_train,y_train,X_test,y_test,feat_names

def main():

     #read in data
    _, _, _, y_test, all_features = load_data()
    
    test_probas = []
    
     # read saved probabilities
    LR_undersampling_test_proba_file = np.loadtxt(LR_undersampling_proba_path, delimiter = ",")
    LR_undersampling_pred_proba = [row[1] for row in LR_undersampling_test_proba_file] 
    test_probas.append(LR_undersampling_pred_proba)
    
    LRl1_undersampling_test_proba_file = np.loadtxt(LRl1_undersampling_proba_path, delimiter = ",")
    LRl1_undersampling_pred_proba = [row[1] for row in LRl1_undersampling_test_proba_file] 
    test_probas.append(LRl1_undersampling_pred_proba)
    
    RF_undersampling_test_proba_file = np.loadtxt(RF_undersampling_proba_path, delimiter = ",")
    RF_undersampling_pred_proba = [row[1] for row in RF_undersampling_test_proba_file] 
    test_probas.append(RF_undersampling_pred_proba)
    
    AB_undersampling_test_proba_file = np.loadtxt(AB_undersampling_proba_path, delimiter = ",")
    AB_undersampling_pred_proba = [row[1] for row in AB_undersampling_test_proba_file] 
    test_probas.append(AB_undersampling_pred_proba)
    
    # compute averages of the assemble
    #avg_test_score = np.mean(test_scores)
    #print(avg_test_score)
    avg_stacking_test_proba = np.mean( np.array( test_probas ), axis=0)

    print("Saving data...")
    # save average probabilities 
    np.savetxt(avg_stacking_proba_path, avg_stacking_test_proba, delimiter=",")

main()

	